import React from 'react';
import { Platform, StatusBar, StyleSheet, View,AsyncStorage } from 'react-native';
import { AppLoading, Asset, Font, Icon } from 'expo';
import AuthLoadingScreen from './src/screens/AuthLoadingScreen'
import {Localization} from 'expo'
import i18n from "i18n-js"
import {en,ru} from './src/Texts/translations'

i18n.fallbacks = true;
i18n.translations = { en, ru };
i18n.locale = Localization.locale;
export default class App extends React.Component {
  state = {
      isLoadingComplete: false,
  };
  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
          return (
              <View style={styles.container}>
                  {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                  <AuthLoadingScreen />
              </View>
          );
      }
    }


  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
          require('./src/assets/images/zen.png'),
          require('./src/assets/audios/test.mp3'),
          require('base-64')
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'space-mono': require('./src/assets/fonts/SpaceMono-Regular.ttf'),
      })
    ])
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    background: '#0000FF',
  },
});

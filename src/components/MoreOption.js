import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image, Platform} from 'react-native';
import {Icon} from 'expo'

const option = (props) => (
    <View>
        <TouchableOpacity onPress={props.onItemPressed} style={styles.listItem}>
            <Image resizeMode="contain" source={require('../assets/icons/icon.png')} style={styles.image} />
            <Text style={styles.text}>{props.optionText}</Text>
            <Icon.Ionicons
                name={Platform.OS === 'ios' ? 'ios-arrow-forward' : 'ios-arrow-forward'}
                color = {'#414141'}
                size={30}
                style={ styles.icon }
            />
        </TouchableOpacity>
    </View>

);

const styles = StyleSheet.create({
    listItem: {
        marginLeft: 10,
        marginRight: 10,
        width: "100%",
        height: 80,
        backgroundColor: "#fff",
        flexDirection: "row",
        alignItems: "center",
        borderColor: "#D1D3D4",
        borderWidth: 0.0,
        borderBottomStyle: "solid",
        borderBottomWidth: 1
    },
    image:{
        width: 26,
        height: 26
    },
    text:{
        marginLeft:15,
        fontSize:16
    }
});

export default option;
import React from 'react'
import {StyleSheet,Text} from 'react-native'
import {Overlay} from 'react-native-elements'
import ThetaButton from './ThetaButton'
import i18n from 'i18n-js'
import {Localization,LinearGradient} from 'expo'

export default OutModal = (props) => (
        <Overlay isVisible={props.isVisible} overlayStyle={{padding:0}} width={300} height={250} borderRadius={40} onBackdropPress={props.goBack} children={
            <LinearGradient
                colors={['#5A2353','#272283']}
                style={styles.main}
            >
                <Text style={styles.text}>{Localization.locale==='ru-RU' ? 'Вы действительно хотите прервать проработку установки?':"Do you really want to interrupt the installation?"}</Text>
                <ThetaButton
                    title={i18n.t('yes')}
                    onPress={props.onYes}
                />
                <ThetaButton
                    title={i18n.t('no')}
                    onPress={props.goBack}
                />
            </LinearGradient>
        }>
        </Overlay>
)
const styles = StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:40
    },
    container:{
        width: 300,
        height: 200,
    },
    text:{
        textAlign:'center',
        color:'#fff',
        fontSize:16
    }
});

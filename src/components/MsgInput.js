import React, { Component } from "react";
import { View, TextInput, StyleSheet,KeyboardAvoidingView } from "react-native";
import {Button} from 'react-native-elements'
import {Icon} from 'expo'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
export default class MsgInput extends Component {
    state = {
        text: ""
    };

    msgChangedHandler = val => {
        this.setState({
            text: val
        });
    };

    msgSubmitHandler = () => {
        if (this.state.text.trim() === "") {
            return;
        }
        this.props.onPress(this.state.text)
        this.state.text=''
        ;
    };

    render() {
        return (
            <View style={styles.inputContainer} enabled>
                <TextInput
                    value={this.state.text}
                    onChangeText={this.msgChangedHandler}
                    style={styles.Input}
                />
                <Button
                    icon={
                        <Icon.Ionicons
                            name="md-send"
                            size={30}
                            color="#C272FF"
                        />
                    }
                    containerStyle={styles.btnContainer}
                    buttonStyle={styles.Button}
                    onPress={this.msgSubmitHandler}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        paddingHorizontal:3,
        width: "100%",
        height:50,
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor:'#fff',
        alignItems: "center"
    },
    Input: {
        borderRadius:15,
        backgroundColor:'#EFEFEF',
        width: "80%"
    },
    btnContainer:{
        width: "20%",
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#fff'
    },
    Button: {
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#fff'
    }
});



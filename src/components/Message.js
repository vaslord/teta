import React,{ Component } from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class Message extends Component {

    render() {
        return(
            <View style={[styles.container,this.props.hasSended ? {alignItems:'flex-end'}:{alignItems:'flex-start'}]}>
                <View style={styles.message}>
                    <Text style={styles.text}>{this.props.text}</Text>
                </View>
            </View>
        );
    }
}


var styles = StyleSheet.create({
    container:{
        flex:1,
        margin:10
    },
    message:{
        flexDirection:'column',
        padding:10,
        maxWidth:wp('70%'),
        borderRadius: 15,
        backgroundColor:'#fff',
    },
    text:{
        fontSize:16,
        color:'#00000',
        textAlign:'left',
        padding:5
    }
});


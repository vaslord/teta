import React,{ Component } from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image, Platform} from 'react-native';
import { Icon,Localization } from 'expo';


export default class ListHealer extends Component {
    ru={
        time:"Часы приема",
        sign:"запись заранее"

    };
    en={
        time:"Time table",
        sign:"writing in advance"
    };
    texts = Localization.locale==="ru-RU"? this.ru:this.en;
    loadImage = ()=>{
        if (this.props.itemImage){
            let imageSize = 60;
            if (this.props.size){
                imageSize = this.props.size
            }
            return (
                <Image
                    resizeMode="contain"
                    source = {this.props.itemImage}
                    style={[styles.itemImage,{height:imageSize,width:imageSize}]}
                />
            );
        } else return null

    };

    render() {
        return(
            <View >
                <TouchableOpacity onPress={this.props.onItemPressed} style={styles.listItem}>
                    {this.loadImage()}
                    <View style={styles.textContainer}>
                        <Text style={{fontSize:16,fontWeight:'bold',color:'#fff'}}>{this.props.name}</Text>
                        <Text style={{fontSize:14,color:'#fff'}}>{this.texts.time} {this.props.time}, {this.props.isAble ? this.texts.sign: ''}</Text>
                        <Text style={{fontSize:13,color:'#fff'}}>{this.props.city}</Text>
                    </View>

                    <Icon.Ionicons
                        name={Platform.OS === 'ios' ? 'ios-arrow-forward' : 'ios-arrow-forward'}
                        color = {'#D0A0FF'}
                        size={30}
                        style={ styles.icon }
                    />
                </TouchableOpacity>
            </View>
        );
    }
}




const styles = StyleSheet.create({
    textContainer:{
        justifyContent:'space-between',
        color:'#fff'
    },
    listItem: {
        padding:5,
        width: "100%",
        height: 80,
        backgroundColor: "transparent",
        flexDirection: "row",
        alignItems: "center",
        borderColor: "#fff",
        borderWidth: 0.0,
        borderBottomWidth: 0.2
    },
    itemImage: {
        marginRight: 8,
    },
    icon: {
        position:"absolute",
        right: 15
    }
});


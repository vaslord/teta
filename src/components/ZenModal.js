import React from 'react';
import { View,Text,Image,StyleSheet,ScrollView } from 'react-native';
import FooterButton from "./FooterButton";
import {Localization,LinearGradient} from 'expo'
import i18n from 'i18n-js'
const ru= {
    text2:"Примите удобное для вас положение и закройте глаза. Сделайте вдох и медленный спокойный выдох. Расслабьтесь. Представьте как в Вас проникает белый искрящийся свет, встраивает данные убеждения и формирует новые нейронные связи в вашем головном мозге. Пронаблюдайте этот процесс до полного завершения. Сделайте вдох и на выдохе откройте глаза.",
    text3:"Примите удобное для вас положение и закройте глаза. Сделайте вдох и медленный  спокойный выдох. Расслабьтесь. Представьте как в Вас проникает белый искрящийся свет и растворяет ваши убеждения, от которых вы хотите освободиться. Пронаблюдайте этот процесс до полного завершения. Сделайте вдох и на выдохе откройте глаза."
};
const en =  {
    text2: "Sit down and close your eyes. Inhale and slowly exhale quietly. Relax. Imagine how white sparkling light penetrates you and dissolves your beliefs from which you want to free yourself. Observe this process until complete. Inhale and exhale open your eyes. ",
    text3: "Sit down and close your eyes. Inhale and slowly exhale quietly. Relax. Imagine how white sparkling light penetrates you and dissolves your beliefs from which you want to free yourself. Observe this process until complete. Inhale and exhale open your eyes. ",
};
const text=Localization.locale==="ru-RU"? ru:en;
const ZenModal = (props) => (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1}}
            >
                    <ScrollView style={styles.msg}>
                        <View style={{alignItems:'center'}}>
                            <Image style={styles.image} source={require('../assets/images/zen.png')} resizeMode='cover'/>
                        </View>
                        <Text style={[styles.text,{fontSize:16}]}>{(props.num===4||props.num==='5')?text.text2:text.text3}</Text>
                    </ScrollView>
                    <FooterButton
                        text={i18n.t('continue')}
                        onPress={props.onPress}
                    />
            </LinearGradient>
);

const styles=StyleSheet.create({
    container:{
        flex: 1,
    },
    text:{
        textAlign:"center",
        margin:15,
        color:"#fff",
        paddingHorizontal:40
    },
    image:{
        alignItems:'center',
        marginBottom:20,
        width: 100,
        height: 90
    },
    button:{
        height:50,
        justifyContent:"center",
        backgroundColor:'#a94eef',
        width:"100%"
    },
    msg:{
        justifyContent:'center',
    }
});
export default ZenModal
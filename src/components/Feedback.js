import React,{ Component } from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';


export default class ListItem extends Component {

    render() {
        return(
            <View style={styles.container}>
                <Image source={this.props.itemImage} resizeMode='contain' style={styles.image}/>
                <View style={styles.feedback}>
                    <Text style={styles.name}>{this.props.name}</Text>
                    <Text style={styles.text}>{this.props.text}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'row',
        margin:10
    },
    feedback:{
        flexDirection:'column',
        width:'85%',
        borderRadius: 15,
        backgroundColor:'#fff',
    },
    image:{
        width:40,
        height:40,
        marginRight: 10
    },
    name:{
        fontSize:14,
        fontWeight:'bold',
        color:'#A94EEF',
        textAlign:'left',
        padding:5
    },
    text:{
        fontSize:16,
        color:'#00000',
        textAlign:'left',
        padding:5
    }
});


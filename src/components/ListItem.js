import React,{ Component } from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image, Platform} from 'react-native';
import { Icon } from 'expo';


export default class ListItem extends Component {
        loadImage = ()=>{
            if (this.props.itemImage){
                let imageSize = 60;
                if (this.props.size){
                    imageSize = this.props.size
                }
                return (
                    <Image
                        resizeMode="contain"
                        source = {this.props.itemImage}
                        style={[styles.itemImage,{height:imageSize,width:imageSize}]}
                    />
                );
            } else return null

        };

    render() {
        return(
                <TouchableOpacity onPress={this.props.onItemPressed} style={styles.listItem}>
                    {this.loadImage()}
                    <Text style={{fontSize:16,color:"#fff"}}>{this.props.itemText}</Text>
                    <Icon.Ionicons
                        name={Platform.OS === 'ios' ? 'ios-arrow-forward' : 'ios-arrow-forward'}
                        color = {'#9291C2'}
                        size={30}
                        style={ styles.icon }
                    />
                </TouchableOpacity>
        );
    }
}




const styles = StyleSheet.create({
    listItem: {
        width: "100%",
        height: 80,
        backgroundColor: "transparent",
        flexDirection: "row",
        alignItems: "center",
        borderColor: "#fff",
        borderWidth: 0.0,
        borderBottomWidth: 0.2,
    },
    itemImage: {
        marginRight: 10,
    },
    icon: {
        position:"absolute",
        right: 15
    }
});


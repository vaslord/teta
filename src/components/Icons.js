import React from 'react';
import { Image,StyleSheet,TouchableOpacity } from 'react-native';
import {Icon} from 'expo'

class HealerIcon extends React.Component {
    render() {
        return (
            <Image
                source={this.props.focused ? require('../assets/icons/active/menu-logo-1.png'): require('../assets/icons/menu-logo-1.png')}                fadeDuration={0}
                style={[styles.icons,this.props.focused ? {color:"#FFF"} : {color:"#9291C2"}]}
            />
        );
    }
}



class DnaIcon extends React.Component {
    render() {
        return (
            <Image
                source={this.props.focused ? require('../assets/icons/active/menu-logo-2.png'): require('../assets/icons/menu-logo-2.png')}                fadeDuration={0}
                style={[styles.icons,this.props.focused ? {color:"#FFF"} : {color:"#9291C2"}]}
            />
        );
    }
}

class LogoIcon extends React.Component {
    render() {
        return (
            <Image
                source={this.props.focused ? require('../assets/icons/active/menu-logo-3.png'): require('../assets/icons/menu-logo-3.png')}                fadeDuration={0}
                style={[styles.icons,this.props.focused ? {color:"#FFF"} : {color:"#9291C2"}]}
            />
        );
    }
}
class FlowerIcon extends React.Component {
    render() {
        return (
            <Image
                source={this.props.focused ? require('../assets/icons/active/menu-logo-4.png'): require('../assets/icons/menu-logo-4.png')}                fadeDuration={0}
                style={[styles.icons,this.props.focused ? {color:"#FFF"} : {color:"#9291C2"}]}
            />
        );
    }
}
class MoreIcon extends React.Component {
    render() {
        return (
            <Image
                source={this.props.focused ? require('../assets/icons/active/menu-logo-5.png'): require('../assets/icons/menu-logo-5.png')}
                fadeDuration={0}
                style={[styles.icons,this.props.focused ? {color:"#FFF"} : {color:"#9291C2"}]}
            />
        );
    }
}
class CloseIcon extends React.Component{
    render() {
        return(
            <TouchableOpacity style={{position:'absolute',
                top:30,
                right:20}}  onPress={this.props.onPress}>
                <Icon.Ionicons
                    name='md-close'
                    size={35}
                    color='#D6D6D6'
                />
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    icons:{
        width: 26,
        height: 26
    }

})

export {LogoIcon,FlowerIcon,HealerIcon,DnaIcon,MoreIcon,CloseIcon}

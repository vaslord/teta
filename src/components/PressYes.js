import React from 'react'
import {View,Text,Image,StyleSheet,ScrollView} from 'react-native'
import {Button} from 'react-native-elements'
import ThetaButton from './ThetaButton'
import i18n from 'i18n-js'
import {LinearGradient} from 'expo'


export default class PressYes extends React.Component {
    showBoldText = ()=>{
        if (this.props.boldText){
            return (
                <Text style={[styles.text,{fontWeight: 'bold'}]}>{this.props.boldText}</Text>
            );
        } else return null
    };
    render() {
    return (
        <LinearGradient
            colors={['#5B2453','#252285']}
            style={styles.main}
        >
            <View style={{alignItems:'center'}}>
                <Image source={require('../assets/images/DNA1.png')} style={styles.image} resizeMode = 'contain' />
            </View>
            <Text style={styles.text}>{this.props.Text}</Text>
            {this.showBoldText()}
            <ThetaButton
                onPress = {this.props.onPressYes}
                title={i18n.t('yes')}
            />
            <ThetaButton
                onPress = {this.props.onPressNo}
                title={i18n.t('no')}
            />
        </LinearGradient>
    )
    }
    };


const styles = StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    image:{
        width:71,
        height:102,
        marginBottom: 40
    },
    button:{
        width: 200,
        height:40,
        fontSize:14,
        borderRadius:20,
        marginTop: 30,
        backgroundColor:'#947BFF'
    },
    text:{
        fontSize: 16,
        color:"#fff",
        textAlign:'center',
        paddingHorizontal:40
    },


});

import React from 'react';
import {View,Image,TouchableOpacity,StyleSheet,Text} from 'react-native'
import { Audio, Icon} from 'expo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class Player  extends React.Component{
    state={
        isPlaying:false
    }
    constructor(props) {
        super(props);
        this.soundObject = new Audio.Sound();
        this.isLoaded = false;
    }
    _onPlayPausePressed = async () => {
        try {
            if(this.state.isPlaying){
                await this.soundObject.pauseAsync();
                this.setState({isPlaying:false})
            }
            else{
                if (this.isLoaded) {
                    await this.soundObject.playAsync();
                    this.setState({isPlaying:true})
                }
                else {
                    await this.soundObject.loadAsync(require('../assets/audios/test.mp3'));
                    await this.soundObject.playAsync();
                    this.isLoaded = true;
                    this.setState({isPlaying:true})
                }
            }

            // Your sound is playing!
        } catch (error) {
            // An error occurred!
        }
    };
    _onStopPressed = () =>{

    };
    render() {
        return (
            <View style={styles.player}>
                <TouchableOpacity
                    style={styles.play}
                    onPress={this._onPlayPausePressed}
                    //disabled={this.state.isLoading}
                    >
                    <Icon.Ionicons
                        name={this.state.isPlaying ? 'md-pause':'md-play'}
                        size={50}
                        color='#707070'
                    />
                </TouchableOpacity>
                <Text style={styles.name}>{this.props.name}</Text>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    player:{
        width:'90%',
        flexDirection:'row',
        height: 70,
        alignItems:'center',
        padding:5,
        marginLeft:15,
        marginRight:15,
        marginTop:10,
        justifyContent:'flex-start',
        backgroundColor: '#Fff',
        borderRadius:15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    play:{
    },
    name:{
        fontSize:14,
        textAlign:'left',
        marginLeft:20
    }

})


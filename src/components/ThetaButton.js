import React from 'react'
import {StyleSheet} from 'react-native'
import {Button} from 'react-native-elements'

export default class ThetaButton extends React.Component {
    render() {
        return (
                <Button
                    disabled={this.props.disabled?this.props.disabled:false}
                    disabledStyle={{backgroundColor:"#5F4E8F"}}
                    disabledTitleStyle={{color:'#fff'}}
                    onPress = {this.props.onPress}
                    containerStyle={{alignItems:'center'}}
                    buttonStyle={[styles.button,{
                        width:this.props.width?this.props.width:200,
                        height:this.props.height?this.props.height:40,
                        backgroundColor:this.props.color?this.props.color:'#5F4E8F'
                    }]}
                    title={this.props.title}
                />
        )
    }
};


const styles = StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    button:{
        fontSize:14,
        borderRadius:20,
        marginTop: 30,
        backgroundColor:'#5F4E8F',
        color:"#fff"
    },
});

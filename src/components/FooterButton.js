import React from 'react';
import { StyleSheet,View } from 'react-native';
import {Button} from 'react-native-elements'

export default FooterButton = (props) => (
    <View
        style={styles.footer}
    >
        <Button
            disabled={props.disabled?props.disabled:false}
            disabledStyle={{backgroundColor:"transparent"}}
            disabledTitleStyle={{color:'#a8a7ce'}}
            onPress={props.onPress}
            buttonStyle={styles.button}
            title={props.text}
        />
    </View>
)

const styles = StyleSheet.create({
    footer:{
        height: 52,
        width:'100%',
        justifyContent: 'center',
        alignItems:'flex-end',
        paddingRight:40,
        backgroundColor:'#514E9D'
    },
    button:{
        backgroundColor: 'Transparent',
        outline:'none',
        fontSize:14,
        border: 'none'
    }
});


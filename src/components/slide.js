import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, View, Text, Image,TouchableOpacity,AsyncStorage} from 'react-native';
import { LinearGradient } from 'expo';
import AppIntroSlider from 'react-native-app-intro-slider';

const styles = StyleSheet.create({
    mainContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    image: {
        width: 100,
        height: 100,
    },
    text: {
        color: 'rgba(255, 255, 255, 0.8)',
        backgroundColor: 'transparent',
        textAlign: 'center',
        paddingHorizontal: 16,
    },
    title: {
        fontSize: 22,
        color: 'white',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
    },
    button: {

    }
});

const slides = [
    {
        key: 'somethun',
        title: 'ThetaSelf',
        text: 'Что-то про лечение!!',
        image: require('../assets/images/dna.png'),
        imageStyle: styles.image,
        colors: ['#6942F2', '#A850FF'],
    },
    {
        key: 'somethun1',
        title: 'ThetaSelf',
        text: 'Что-то про лечение!!',
        image: require('../assets/images/structure.png'),
        imageStyle: styles.image,
        colors: ['#6942F2', '#A850FF'],
    },
    {
        key: 'somethun2',
        title: 'ThetaSelf',
        text: 'Что-то про лечение!!',
        image: require('../assets/images/atom.png'),
        imageStyle: styles.image,
        colors: ['#6942F2', '#A850FF'],
    },
];


export default class Slides extends React.Component {
    _signInAsync = async () => {
        await AsyncStorage.setItem('userToken', 'abc');
        this.props.navigation.navigate('App');
    };
    _renderItem = props => (
        <LinearGradient
            style={[styles.mainContent, {
                paddingTop: props.topSpacer,
                paddingBottom: props.bottomSpacer,
                width: props.width,
                height: props.height,
            }]}
            colors={props.colors}
            start={{x: 0, y: .1}} end={{x: .1, y: 1}}
        >
            <Image style={{ backgroundColor: 'transparent' }} source={props.image} size={200} color="white" />
            <View>
                <Text style={styles.title}>{props.title}</Text>
                <Text style={styles.text}>{props.text}</Text>
            </View>
        </LinearGradient>
    );

    render() {
        return (
                <AppIntroSlider
                    slides={slides}
                    renderItem={this._renderItem}
                    bottomButton
                    onDone={this._signInAsync}
                />

        );
    }
}
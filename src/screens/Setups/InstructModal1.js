import React from 'react';
import ZenModal from '../../components/ZenModal'
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'
import {View} from 'react-native'

export default class InstructModal1 extends React.Component {

    constructor(props){
        super(props);
        let key = this.props.navigation.getParam('key','1');
        this.state={
            isVisible:false,
            key:key
        };
    }
    onPress = () => {
        if ((this.state.key===1)||(this.state.key===2)) this.props.navigation.push('EnterModal',{
            key : this.state.key
        });
        else this.props.navigation.push('FinishModal',{
            key:this.state.key
        })
    };
    render() {
        return (
            <View style={{flex:1}}>
                <ZenModal
                    onPress={this.onPress}
                    num={this.state.key}
                />
                <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
            </View>
        );



    }
}




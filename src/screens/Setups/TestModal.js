import React, { Component } from 'react';
import { View,StyleSheet,Text,ScrollView,TouchableOpacity } from 'react-native';
import SwitchButton from 'switch-button-react-native';
import {Button} from 'react-native-elements'
export default class TestModal extends React.Component {
    constructor() {
        super();

        this.state = {
            activeSwitch: 1
        };
    }


    render() {

        return (

            <ScrollView style={styles.container}>
                <Text style={styles.title}>Тестирование</Text>
                <View style={styles.testcontainer}>
                    <Text style={styles.question}>1.Я прав?</Text>
                    <SwitchButton
                        onValueChange={(val) => this.setState({activeSwitch: val})}      // this is necessary for this component
                        text1='Да'                        // optional: first text in switch button --- default ON
                        text2='Нет'                       // optional: second text in switch button --- default OFF
                        switchWidth={200}                 // optional: switch width --- default 44
                        switchHeight={45}                 // optional: switch height --- default 100
                        switchdirection='ltr'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                        switchBorderRadius={15}          // optional: switch border radius --- default oval
                        switchSpeedChange={500}           // optional: button change speed --- default 100
                        switchBorderColor='#d1d3d4'       // optional: switch border color --- default #d4d4d4
                        switchBackgroundColor='#fff'      // optional: switch background color --- default #fff
                        btnBorderColor='#fff'          // optional: button border color --- default #00a4b9
                        btnBackgroundColor='#fff'      // optional: button background color --- default #00bcd4
                        fontColor='#414141'               // optional: text font color --- default #b1b1b1
                        activeFontColor='#A94EEF'            // optional: active font color --- default #fff
                    />
                </View>

                <View style={styles.testcontainer}>
                    <Text style={styles.question}>1.Я прав?</Text>
                    <SwitchButton
                        onValueChange={(val) => this.setState({activeSwitch: val})}      // this is necessary for this component
                        text1='Да'                        // optional: first text in switch button --- default ON
                        text2='Нет'                       // optional: second text in switch button --- default OFF
                        switchWidth={200}                 // optional: switch width --- default 44
                        switchHeight={45}                 // optional: switch height --- default 100
                        switchdirection='ltr'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                        switchBorderRadius={15}          // optional: switch border radius --- default oval
                        switchSpeedChange={500}           // optional: button change speed --- default 100
                        switchBorderColor='#d1d3d4'       // optional: switch border color --- default #d4d4d4
                        switchBackgroundColor='#fff'      // optional: switch background color --- default #fff
                        btnBorderColor='#fff'          // optional: button border color --- default #00a4b9
                        btnBackgroundColor='#fff'      // optional: button background color --- default #00bcd4
                        fontColor='#414141'               // optional: text font color --- default #b1b1b1
                        activeFontColor='#A94EEF'            // optional: active font color --- default #fff
                    />
                </View>

                <View style={styles.testcontainer}>
                    <Text style={styles.question}>1.Я прав?</Text>
                    <SwitchButton
                        onValueChange={(val) => this.setState({activeSwitch: val})}      // this is necessary for this component
                        text1='Да'                        // optional: first text in switch button --- default ON
                        text2='Нет'                       // optional: second text in switch button --- default OFF
                        switchWidth={200}                 // optional: switch width --- default 44
                        switchHeight={45}                 // optional: switch height --- default 100
                        switchdirection='ltr'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                        switchBorderRadius={15}          // optional: switch border radius --- default oval
                        switchSpeedChange={500}           // optional: button change speed --- default 100
                        switchBorderColor='#d1d3d4'       // optional: switch border color --- default #d4d4d4
                        switchBackgroundColor='#fff'      // optional: switch background color --- default #fff
                        btnBorderColor='#fff'          // optional: button border color --- default #00a4b9
                        btnBackgroundColor='#fff'      // optional: button background color --- default #00bcd4
                        fontColor='#414141'               // optional: text font color --- default #b1b1b1
                        activeFontColor='#A94EEF'            // optional: active font color --- default #fff
                    />
                </View>

                <View style={styles.testcontainer}>
                    <Text style={styles.question}>1.Я прав?</Text>
                    <SwitchButton
                        onValueChange={(val) => this.setState({activeSwitch: val})}      // this is necessary for this component
                        text1='Да'                        // optional: first text in switch button --- default ON
                        text2='Нет'                       // optional: second text in switch button --- default OFF
                        switchWidth={200}                 // optional: switch width --- default 44
                        switchHeight={45}                 // optional: switch height --- default 100
                        switchdirection='ltr'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                        switchBorderRadius={15}          // optional: switch border radius --- default oval
                        switchSpeedChange={500}           // optional: button change speed --- default 100
                        switchBorderColor='#d1d3d4'       // optional: switch border color --- default #d4d4d4
                        switchBackgroundColor='#fff'      // optional: switch background color --- default #fff
                        btnBorderColor='#fff'          // optional: button border color --- default #00a4b9
                        btnBackgroundColor='#fff'      // optional: button background color --- default #00bcd4
                        fontColor='#414141'               // optional: text font color --- default #b1b1b1
                        activeFontColor='#A94EEF'            // optional: active font color --- default #fff
                    />
                </View>

                <View style={styles.testcontainer}>
                    <Text style={styles.question}>1.Я прав?</Text>
                    <SwitchButton
                        onValueChange={(val) => this.setState({activeSwitch: val})}      // this is necessary for this component
                        text1='Да'                        // optional: first text in switch button --- default ON
                        text2='Нет'                       // optional: second text in switch button --- default OFF
                        switchWidth={200}                 // optional: switch width --- default 44
                        switchHeight={45}                 // optional: switch height --- default 100
                        switchdirection='ltr'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                        switchBorderRadius={15}          // optional: switch border radius --- default oval
                        switchSpeedChange={500}           // optional: button change speed --- default 100
                        switchBorderColor='#d1d3d4'       // optional: switch border color --- default #d4d4d4
                        switchBackgroundColor='#fff'      // optional: switch background color --- default #fff
                        btnBorderColor='#fff'          // optional: button border color --- default #00a4b9
                        btnBackgroundColor='#fff'      // optional: button background color --- default #00bcd4
                        fontColor='#414141'               // optional: text font color --- default #b1b1b1
                        activeFontColor='#A94EEF'            // optional: active font color --- default #fff
                    />
                </View>

                <View style={styles.testcontainer}>
                    <Text style={styles.question}>1.Я прав?</Text>
                    <SwitchButton
                        onValueChange={(val) => this.setState({activeSwitch: val})}      // this is necessary for this component
                        text1='Да'                        // optional: first text in switch button --- default ON
                        text2='Нет'                       // optional: second text in switch button --- default OFF
                        switchWidth={200}                 // optional: switch width --- default 44
                        switchHeight={45}                 // optional: switch height --- default 100
                        switchdirection='ltr'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                        switchBorderRadius={15}          // optional: switch border radius --- default oval
                        switchSpeedChange={500}           // optional: button change speed --- default 100
                        switchBorderColor='#d1d3d4'       // optional: switch border color --- default #d4d4d4
                        switchBackgroundColor='#fff'      // optional: switch background color --- default #fff
                        btnBorderColor='#fff'          // optional: button border color --- default #00a4b9
                        btnBackgroundColor='#fff'      // optional: button background color --- default #00bcd4
                        fontColor='#414141'               // optional: text font color --- default #b1b1b1
                        activeFontColor='#A94EEF'            // optional: active font color --- default #fff
                    />
                </View>

                <View style={styles.testcontainer}>
                    <Text style={styles.question}>1.Я прав?</Text>
                    <SwitchButton
                        onValueChange={(val) => this.setState({activeSwitch: val})}      // this is necessary for this component
                        text1='Да'                        // optional: first text in switch button --- default ON
                        text2='Нет'                       // optional: second text in switch button --- default OFF
                        switchWidth={200}                 // optional: switch width --- default 44
                        switchHeight={45}                 // optional: switch height --- default 100
                        switchdirection='ltr'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                        switchBorderRadius={15}          // optional: switch border radius --- default oval
                        switchSpeedChange={500}           // optional: button change speed --- default 100
                        switchBorderColor='#d1d3d4'       // optional: switch border color --- default #d4d4d4
                        switchBackgroundColor='#fff'      // optional: switch background color --- default #fff
                        btnBorderColor='#fff'          // optional: button border color --- default #00a4b9
                        btnBackgroundColor='#fff'      // optional: button background color --- default #00bcd4
                        fontColor='#414141'               // optional: text font color --- default #b1b1b1
                        activeFontColor='#A94EEF'            // optional: active font color --- default #fff
                    />
                </View>
                <Button
                    onPress = {() => {this.props.navigation.navigate('YesModal1')}}
                    containerStyle={{alignItems:'center'}}
                    buttonStyle={styles.button}
                    title='Завершить тест'
                    linearGradientProps={{
                        colors: ['#C272FF', '#947bff'],
                        start: { x: 0, y: 0.5 },
                        end: { x: 1, y: 0.5 },
                    }}
                />
            </ScrollView>

        );
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
    },
    title:{
        textAlign:'center',
        color:"#A94EEF",
        fontSize:20,
        marginTop:40
    },
    question:{
        textAlign:'center',
        fontSize:14,
        marginBottom: 20
    },
    testcontainer:{
        flex:1,
        alignItems:'center',
        marginTop: 20
    },
    button:{
        width: 200,
        height: 40,
        fontSize:14,
        borderRadius:20,
        marginVertical: 40,

    },
})
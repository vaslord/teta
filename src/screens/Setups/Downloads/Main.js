import React from 'react';
import {  StyleSheet,FlatList } from 'react-native';
import ListItem from '../../../components/ListItem'
import {LinearGradient,Localization} from 'expo'

export default class Downloads extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: Localization.locale==='ru-RU'?"Загрузки":"Downloads",
        };
    };
    worksRu = require("../../../Texts/myWorks").works;
    worksEn = require("../../../TextsEn/myWorks").works;
    works = Localization.locale==="ru-RU"? this.worksRu:this.worksEn;
    _OnPress = (texts) => (
        this.props.navigation.push('MyWorks',{
            texts:texts
        })
    );
    render() {
        return (
            <LinearGradient style={{flex:1}}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <FlatList style={styles.container}
                          data={this.works}
                          renderItem={
                              ({item})=>
                                  <ListItem
                                      itemImage = {false}
                                      itemText = {item.text}
                                      onItemPressed = {()=>this._OnPress(item.texts)}
                                  />
                          }
                />
            </LinearGradient>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10,
        paddingLeft: 10,
        backgroundColor:"transparent"
    },

});

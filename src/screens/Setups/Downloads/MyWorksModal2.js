import React from 'react';
import { View,Text,Image,StyleSheet,ScrollView } from 'react-native';
import FooterButton from "../../../components/FooterButton";
import {Localization,LinearGradient} from 'expo'
import i18n from 'i18n-js'
export default class MyWorksModal2 extends React.Component {
    state = {
        isFirst : true
    };
    onPress = () => {
        this.props.navigation.navigate('Spheres')
    }
    ru={
        text:"Теперь Вас ждет новый образ мышления и качество жизни. Встречайте и проявляйте нового Себя осознанно и с радостью!",
        title: "Мы поздравляем Вас!"
    };
    en =  {

        text: "Now you are waiting for a new way of thinking and quality of life. Meet and manifest your New Self consciously and with joy!",
        title: "We congratulate you!"
    };
    text=Localization.locale==="ru-RU"? this.ru:this.en;
    render() {
        return (
            <LinearGradient style={styles.container}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >

                    <ScrollView style={styles.msg}>
                        <Text style={[styles.text,{fontSize:24,fontWeight:'bold'}]}>{this.text.title}</Text>
                        <View style={{alignItems:'center'}}>
                            <Image style={styles.image} source={require('../../../assets/images/sauna.png')}/>
                        </View>
                        <Text style={[styles.text,{fontSize:16}]}>{this.text.text}</Text>
                    </ScrollView>
                    <FooterButton
                        text={i18n.t('finish')}
                        onPress={this.onPress}
                    />
            </LinearGradient>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1,
    },
    text:{
        textAlign:"center",
        margin:15,
        paddingHorizontal:40,
        color:'#fff'
    },
    image:{
        alignItems:'center',
        marginBottom:20,
        width: 100,
        height: 90
    },
    button:{
        height:50,
        justifyContent:"center",
        backgroundColor:'#a94eef',
        width:"100%"
    },
    msg:{
        justifyContent:'center',
    }
})



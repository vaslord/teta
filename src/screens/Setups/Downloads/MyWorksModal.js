import React from 'react';
import ZenModal from '../../../components/ZenModal'

export default class MyWorksModal extends React.Component {
    state = {
        isFirst : true
    };
    onPress = () => {
        this.props.navigation.navigate('MyWorksModal2')
    };
    render() {
            return (
                <ZenModal
                    num="5"
                    onPress={this.onPress}
                />

        );
    }
}




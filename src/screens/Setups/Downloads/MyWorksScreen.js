import React from 'react'
import {  StyleSheet, View,Text,FlatList } from 'react-native';
import ThetaButton from '../../../components/ThetaButton'
import i18n from "i18n-js"
import {LinearGradient,Localization} from 'expo'
export default class MyWorksScreen extends React.Component{
    static navigationOptions = {
        title: 'Мои проработки',
    };

    onItemPressed = () => this.props.navigation.navigate('MyModal');
    render() {
        const itemTexts = this.props.navigation.getParam('texts', 'No-text');
        return (
            <LinearGradient style={styles.main}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <FlatList
                    style={styles.listContainer}
                    data={itemTexts}
                    renderItem={
                        ({item})=>
                            <Text style={styles.text}>{item}</Text>
                    }
                    ListFooterComponent = {
                        <View style={styles.buttonsContainer}>
                            <Text style={styles.title}>{Localization.locale==='ru-RU'?'Хотите ли Вы принять эти установки?':'Do you want to accept these settings?'}</Text>
                            <ThetaButton
                                onPress={()=>this.props.navigation.navigate('MyModal')}
                                color='#514E9D'
                                title={i18n.t('yes')}
                            />
                            <ThetaButton
                                onPress={()=>this.props.navigation.navigate('MyModal')}
                                color='#514E9D'
                                title={i18n.t('no')}
                            />
                        </View>
                    }
                    />
            </LinearGradient>
        );
    }

};



const styles = StyleSheet.create({
    main:{
        flex: 1
    },
    listContainer: {
        flex:1,
        justifyContent:'space-between',
        margin:15,
    },
    text:{
        marginBottom: 15,
        fontSize:14,
        color:'#fff'
    },
    title:{
        color:'#fff',
        fontSize:24,
        marginVertical:5,
        marginHorizontal:20,
        textAlign:'center'
    },
    buttonsContainer:{
        alignItems:'center',
        marginVertical: 40
    },
});

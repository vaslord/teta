import React from 'react';
import { View, StyleSheet,AsyncStorage,FlatList,Text,TextInput,ScrollView } from 'react-native';
import i18n from 'i18n-js';
import {LinearGradient} from 'expo'
import {CloseIcon} from "../../../components/Icons";
import OutModal from '../../../components/OutModal';
import FooterButton from '../../../components/FooterButton';
export default class HealthScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isVisible:false,text:'' };
    }
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'ThetaSelf'),
        };
    };

    onPress= async ()=>{
        await AsyncStorage.setItem('subcat1',this.state.text);
        await AsyncStorage.setItem('subcat2',this.state.text.toLowerCase());
        await AsyncStorage.setItem('subcat3','жить без такого диагноза как '+this.state.text.toLowerCase());
        await this.props.navigation.navigate('subCat1',{
            cat:this.state.text.toLowerCase()
        })
    };
    render() {
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1}}
            >
                <ScrollView style={styles.msg}>
                    <Text style={styles.text}>Введите ваш диагноз:</Text>
                    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                        <Text style={[styles.text,{backgroundColor: 'transparent',marginTop:20}]}>У меня есть </Text>
                        <TextInput
                            style={{ backgroundColor: 'transparent',paddingHorizontal:10,color:'#fff',marginTop:20,fontSize:16,textAlign:"center",}}
                            onChangeText={(text) => this.setState(prevState=>({
                                    ...prevState,
                                    text:text
                                })
                            )}
                            value={this.state.text}
                            placeholder={this.state.text}
                            placeholderTextColor = "#fff"
                            multiline
                            autoFocus
                            blurOnSubmit
                        />
                    </View>

                </ScrollView>
                <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
                <FooterButton
                    disabled = {!this.state.text}
                    onPress={this.onPress}
                    text={i18n.t('continue')}
                />
            </LinearGradient>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    msg:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    text:{
        fontSize:16,
        textAlign:'center',
        color:"#fff"
    }
});

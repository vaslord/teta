import React from 'react';
import { View, StyleSheet,ScrollView,FlatList } from 'react-native';
import ListItem from '../../../components/ListItem'
import {LinearGradient} from 'expo'

export default class Relationships extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'TetaSelf'),
        };
    };

    onPress = (subList,title) => {
        this.props.navigation.push('Parents',{
            subList : subList,
            title: title
        })
    };
    render() {
        const sphere = this.props.navigation.getParam('sphere','error');
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1}}
            >
                <FlatList style={styles.container}
                          data={sphere}
                          renderItem={
                              ({item})=>{
                                  return <ListItem
                                      itemImage = {false}
                                      itemText = {item.title}
                                      onItemPressed = {() => this.onPress(item.inside,item.title)}
                                  />}
                          }
                />
            </LinearGradient>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        paddingLeft: 20,
        backgroundColor:"transparent"
    },

});

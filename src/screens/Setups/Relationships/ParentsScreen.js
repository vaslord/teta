import React from 'react';
import { View, StyleSheet,AsyncStorage,FlatList } from 'react-native';
import ListItem from '../../../components/ListItem'
import i18n from 'i18n-js';
import {LinearGradient} from 'expo'

export default class ParentsScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'ThetaSelf'),
        };
    };

    onPress= async (item)=>{
        await AsyncStorage.setItem('subcat1',item.title);
        await AsyncStorage.setItem('subcat2',item.title2?item.title2:item.title.toLowerCase());
        await AsyncStorage.setItem('subcat3',item.title3?item.title3:item.title.toLowerCase());
        await this.props.navigation.navigate('subCat1',{
            cat:item.title.toLowerCase()
        })
    };
    render() {
        const subList = this.props.navigation.getParam('subList','error');
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1}}
            >
                <FlatList style={styles.container}
                          data={subList}
                          renderItem={
                              ({item})=>{
                                  return <ListItem
                                      itemImage = {false}
                                      itemText = {item.title}
                                      onItemPressed = {()=>this.onPress(item)}
                                  />}
                          }
                />
            </LinearGradient>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        paddingLeft:20,
        backgroundColor:"#transparent"
    },

});

import React from 'react';
import { Text,StyleSheet } from 'react-native';
import ThetaButton from "../../components/ThetaButton";
import {Localization,LinearGradient} from 'expo'
import i18n from 'i18n-js'
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'
export default class subCat2a extends React.Component {
    state = {
        isVisible:false
    };
    render() {
        const text = this.props.navigation.getParam('cat','проблема');
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={styles.container}
            >
                <Text style={[styles.text,{marginBottom:10}]}>{Localization.locale==='ru-RU'?"Хотите ли вы отказаться от этого убеждения?":"Do you want to give up this belief?"}</Text>
                <ThetaButton
                    title = {i18n.t('yes')}
                    onPress = {()=>this.props.navigation.push('InstructModal1',{
                        key:1
                    })}
                />
                <ThetaButton
                    title = {i18n.t('no')}
                    onPress = {()=>this.props.navigation.push('EnterModal',{
                        key:1
                    })}
                />
                <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
            </LinearGradient>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
    text:{
        textAlign:"center",
        color:"#fff",
        fontSize: 20
    }
});



import React from 'react';
import { Text,StyleSheet,View } from 'react-native';

import {Localization,LinearGradient} from 'expo'
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'
import ThetaButton from "../../components/ThetaButton";
export default class subCat3 extends React.Component {
    state = {
        isVisible:false
    };
    onPress = () => {
        let key= this.props.navigation.getParam('key','1');
        if (key===1)this.props.navigation.push('VideoModal',{
            key:2
        });
        else this.props.navigation.push('VideoModal',{
            key:3
        });
    };
    onPress2 = () => {
        let key= this.props.navigation.getParam('key','1');
        if (key===1)this.props.navigation.push('YesModal1',{
            key:1
        });
        else this.props.navigation.push('YesModal1',{
            key:2
        });
    };
    render() {
        const text = this.props.navigation.getParam('cat','Вы не ответили на вопрос');
        const subCat = this.props.navigation.getParam('subCat','Ошибка');
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={styles.container}
            >
                <View style={styles.msg}>
                    <Text style={styles.text}>{Localization.locale==="ru-RU"?("Проверьте, пожалуйста, с помощью теста убеждения: "):("Please check with the belief test: ")}</Text>
                    <Text style={styles.text}>{text}</Text>
                </View>
                <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
                <ThetaButton
                    title={Localization.locale==="ru-RU"?"ВИДЕО, КАК ПРОВЕСТИ ТЕСТ":"WATCH THE VIDEO HOW TO TEST"}
                    width={330}
                    onPress={this.onPress}
                />
                <ThetaButton
                    title={Localization.locale==="ru-RU"?"ПРОДОЛЖИТЬ":"CONTINUE"}
                    width={330}
                    onPress={this.onPress2}
                />
                <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
            </LinearGradient>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
    text:{
        textAlign:"center",
        color:"#fff",
        marginBottom:20
    },
    msg:{
        alignItems:'center',
        justifyContent:'center'
    }
})



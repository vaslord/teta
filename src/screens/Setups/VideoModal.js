import React from 'react';
import { View,Text,TouchableOpacity,StyleSheet,ScrollView } from 'react-native';
import FooterButton from "../../components/FooterButton";
import {Video,Localization,LinearGradient} from 'expo'
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'

export default class VideoModal extends React.Component {
    onPress = () => {
        let key = this.props.navigation.getParam('key','1');
        this.setState({shouldPlay:false});
        if(key===1) this.props.navigation.push('subCat2',{
            cat: this.props.navigation.getParam('cat','error')
        });
        else if(key===2) this.props.navigation.push('YesModal1',{
            key :1
        });
        else  this.props.navigation.push('YesModal1',{
            key :2
            });
    };
    state = {
        shouldPlay: true,
        isVisible:false
    };
    handlePlayAndPause = () => {
            this.setState((prevState) => ({shouldPlay: !prevState.shouldPlay}));
    };
    render() {
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1}}
            >
                <ScrollView style={styles.msg}>
                    <Text style={[styles.text,{fontSize:20}]}>{Localization.locale==="ru-RU"?"Как провести тестирование?":"How to test?"}</Text>
                    <View style={{alignItems:'center',marginBottom:20}}>
                        <TouchableOpacity onPress={this.handlePlayAndPause}>
                            <Video
                                source={require('../../assets/videos/instruction.mp4')}
                                isLooping
                                useNativeControls={true}
                                rate={1.0}
                                volume={1.0}
                                isMuted={false}
                                resizeMode="contain"
                                style={styles.video}
                                shouldPlay={this.state.shouldPlay}
                            />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
                <FooterButton
                    text={Localization.locale==="ru-RU"?"Продолжить":"Continue"}
                    onPress={this.onPress}
                />
            </LinearGradient>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1,
    },
    text:{
        textAlign:"center",
        margin:15,
        color:"#fff"
    },
    image:{
        alignItems:'center',
        marginBottom:20,
        width: 100,
        height: 90
    },
    button:{
        height:50,
        justifyContent:"center",
        backgroundColor:'#a94eef',
        width:"100%"
    },
    msg:{
        justifyContent:'center',
    },
    video: {
        width: 250,
        height: 400,
        margin: 40
    },
})



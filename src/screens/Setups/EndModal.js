import React from 'react';
import ThetaButton from '../../components/ThetaButton'
import {Localization,LinearGradient} from 'expo';
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'

export default class EndModal extends React.Component {
    state ={
        isVisible:false
    }
    onHelp = () =>{
        this.props.navigation.navigate('Home'
        )
    };
    onBuy= () =>{
        this.props.navigation.push('Works')
    };
    render() {
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1,alignItems:'center',justifyContent:'center'}}
            >
                <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
                <ThetaButton
                    onPress ={this.onHelp}
                    title = {Localization.locale==='ru-RU'?'ОБРАТИТЬСЯ К ТЕТА-ХИЛЕРУ':
                        'CONTACT THETA HEALER'}
                    width={265}
                />
                <ThetaButton
                    onPress ={this.onBuy}
                    title = {Localization.locale==='ru-RU'?'КУПИТЬ ПЛАТНУЮ ВЕРСИЮ':
                        'BUY SUBSCRIPTION'}
                    width={265}
                />
            </LinearGradient>
        );
    }
}
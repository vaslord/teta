import React from 'react';
import { Text,StyleSheet,AsyncStorage,ScrollView } from 'react-native';
import FooterButton from "../../components/FooterButton";
import {Localization,LinearGradient} from 'expo'
import i18n from 'i18n-js'
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'
export default class subCat4 extends React.Component {
    constructor() {
        super();
        this.state = {
            isLoading : true,
            isVisible : false
        };
        this._loadAsync();
    }
    _loadAsync= async ()=>{
        const t1 = await AsyncStorage.getItem('subcat1');
        const t2 = await AsyncStorage.getItem('subcat2');
        const t3 = await AsyncStorage.getItem('subcat3');
        const f2 = await AsyncStorage.getItem('form2');
        await this.setState({
            isVisible:false,
            isLoading: false,
            subCat: t1,
            form2:f2,
            subCat2:t2,
            subCat3:t3
        })
    };
    onPress = () => {
        this.props.navigation.push('YesModal1',{
            key:3
        });
    };
    render() {
        if (this.state.isLoading)
            return(
                <LinearGradient
                    colors={['#5B2453','#252285']}
                    style={styles.container}
                >
                </LinearGradient>
            )
        else return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1}}
            >
                <ScrollView style={styles.container}>
                    <Text style={styles.text}>
                        {Localization.locale==='ru-RU'?('Я знаю как жить без '+ this.state.subCat2):('I know how to live without ') + this.state.subCat2}
                    </Text>
                    <Text style={styles.text}>
                        {Localization.locale==='ru-RU'?('Я знаю как '+ this.state.form2):(
                            'I know how to '+ this.state.form2)}
                    </Text>
                    <Text style={styles.text}>
                        {Localization.locale==='ru-RU'?('Я знаю,как ' + this.state.subCat3):('I know how to ') + this.state.subCat3}
                    </Text>
                    <Text style={styles.text}>
                        {Localization.locale==='ru-RU'?('Я знаю, как это чувствуется '+ this.state.subCat3):('I know how it feels ') + this.state.subCat3}
                    </Text>
                </ScrollView>
                <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
                <FooterButton
                    onPress={this.onPress}
                    text={i18n.t('continue')}
                />
            </LinearGradient>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1,
        padding:20,
        alignItems:'center',
        justifyContent:'center'
    },
    text:{
        textAlign:"center",
        color:"#fff",
        marginBottom:20,
        fontSize:14
    }
})



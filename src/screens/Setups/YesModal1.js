import React from 'react';
import PressYes from '../../components/PressYes'
import {Localization} from 'expo';
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'
import {View} from 'react-native'

export default class YesModal1 extends React.Component {
    constructor(props){
        super(props);
        let key = props.navigation.getParam('key','1');
        this.state={
            isVisible:false,
            key:key
        };
        if (this.state.key===3) this.state.text={
            ru:"Хотите ли Вы принять эти установки?"
        };
        else if(this.state.key===2) this.state.text={
            ru:"Хотите ли Вы отказаться от этой установки?"
        };
        else this.state.text = {
            ru:"Хотите ли вы избавится от этой установки?"
        }

    }
    onPressYes = () =>{
        let key;
        if (this.state.key===1) key=2;
        else if(this.state.key===2)key = 3;
        else key = 4;
        this.props.navigation.push('InstructModal1',{
            key:key
        })
    };
    onPressNo = () =>{
        if (this.state.key===1) this.props.navigation.push('EnterModal',{
            key:2
        });
        if (this.state.key===2) this.props.navigation.push('FinishModal',{
            key:3
        });
        if (this.state.key===3) this.props.navigation.push('FinishModal',{
            key:4
        })
    };
    render() {
        return (
            <View style={{flex:1}}>
                <PressYes
                    onPressYes={this.onPressYes}
                    onPressNo={this.onPressNo}
                    Text={Localization.locale==="ru-RU"?this.state.text.ru:"Do you want to get rid of this installation?"}
                />
                <CloseIcon onPress={()=> this.setState(prevState=>({...prevState,isVisible:true}))}/>
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
            </View>

        );
    }
}





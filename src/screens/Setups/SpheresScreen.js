import React, { Component } from "react";
import { StyleSheet, View,Image,TouchableOpacity,Text } from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import i18n from "i18n-js"
import { Localization,LinearGradient } from 'expo';
import {Button,Icon} from 'react-native-elements'

export default class SpheresScreen extends Component {
    static navigationOptions = ({ navigation }) =>  {
        return {
            title: "ThetaSelf",
            headerLeft: (
                <Button
                    icon={<Image
                        source={require('../../assets/icons/play-button.png')}
                        style={{width:30,height:30}}
                    />}
                    buttonStyle={{backgroundColor:'transparent'}}
                    onPress={() => navigation.navigate('Settings')}
                />
            )
        }
    };
    relationshipsRu = require("./src/Relationships").relationships;
    healthRu = require("./src/health").health;
    fearsRu = require("./src/fears").fears;
    moneyRu = require("./src/money").money;
    selfRu = require("./src/self").self;
    relationshipsEn = require("./srcEn/Relationships").relationships;
    healthEn = require("./srcEn/health").health;
    fearsEn = require("./srcEn/fears").fears;
    moneyEn = require("./srcEn/money").money;
    selfEn = require("./srcEn/self").self;
    health = Localization.locale==="ru-RU"?this.healthRu:this.healthEn;
    fears = Localization.locale==="ru-RU"?this.fearsRu:this.fearsEn;
    money = Localization.locale==="ru-RU"?this.moneyRu:this.moneyEn;
    relationships = Localization.locale==="ru-RU"?this.relationshipsRu:this.relationshipsEn;
    self = Localization.locale==="ru-RU"?this.selfRu:this.selfEn;
    _OnPressedRelationships = () => { this.props.navigation.push('Relationships',{
        sphere: this.relationships,
        title: i18n.t('relates')
    })};
    _OnPressedSelf = () => { this.props.navigation.push('Relationships',{
        sphere: this.self,
        title: i18n.t('self')
    })};
    _OnPressedFear = () => { this.props.navigation.push('Parents',{
        subList: this.fears,
        title: i18n.t('fears')
    })};
    _OnPressedHealth = () => { this.props.navigation.push('Health',{
        title: i18n.t('health')
    })};
    _OnPressedMoney = () => { this.props.navigation.push('Relationships',{
        sphere: this.money,
        title: i18n.t('money')
    })};
    _OnPressedDownloads = () =>{ this.props.navigation.navigate('Downloads')};
    render() {
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={styles.container}
            >
                <View style={styles.row1}>
                    <TouchableOpacity onPress={this._OnPressedHealth} style={styles.health} elevation={5}>
                        <Image
                            resizeMode="contain"
                            source = {require('../../assets/icons/main-icon-5.png')}
                            style={styles.itemImage}
                        />
                        <Text style={styles.text}>{i18n.t('health')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this._OnPressedFear} style={styles.fear}>
                        <Image
                            resizeMode="contain"
                            source = {require('../../assets/icons/main-icon-4.png')}
                            style={styles.itemImage}
                        />
                        <Text style={styles.text}>{i18n.t('fears')}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.row2}>
                    <TouchableOpacity onPress={this._OnPressedRelationships} style={styles.relationships}>
                        <Image
                            resizeMode="contain"
                            source = {require('../../assets/icons/main-icon-1.png')}
                            style={styles.itemImage}
                        />
                        <Text style={styles.text}>{i18n.t('relates')}</Text>
                    </TouchableOpacity>
                    <View style={styles.spheres}>
                        <Image
                            resizeMode="contain"
                            source = {require('../../assets/images/avatar.png')}
                            style={styles.avatar}
                        />
                    </View>
                    <TouchableOpacity onPress={this._OnPressedDownloads} style={styles.downloads}>
                        <Image
                            resizeMode="contain"
                            source = {require('../../assets/icons/main-icon-6.png')}
                            style={styles.itemImage}
                        />
                        <Text style={styles.text}>{i18n.t('downloads')}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.row3}>
                    <TouchableOpacity onPress={this._OnPressedMoney} style={styles.money}>
                        <Image
                            resizeMode="contain"
                            source = {require('../../assets/icons/main-icon-2.png')}
                            style={styles.itemImage}
                        />
                        <Text style={styles.text}>{i18n.t('money')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this._OnPressedSelf} style={styles.self}>
                        <Image
                            resizeMode="contain"
                            source = {require('../../assets/icons/main-icon-3.png')}
                            style={styles.itemImage}
                        />
                        <Text style={styles.text}>{i18n.t('self')}</Text>
                    </TouchableOpacity>
                </View>
            </LinearGradient>
        );
    }
}
const a = wp('27%');
const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection:'row'
    },
    row1:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    row2:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    row3:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    relationships: {
        width: a,
        height: a,
        backgroundColor: "#573976",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#9291C2",
        borderWidth: 4,
        borderRadius: a/2.0,
        marginBottom:40
    },
    health: {
        marginBottom:40,
        width: a,
        height: a,
        backgroundColor: "#573976",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#9291C2",
        borderWidth: 4,
        borderRadius: a/2.0,

    },
    fear: {
        width: a,
        height: a,
        backgroundColor: "#573976",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#9291C2",
        borderWidth: 4,
        borderRadius: a/2.0,
    },
    money: {
        width: a,
        height: a,
        backgroundColor: "#573976",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#9291C2",
        borderWidth: 4,
        borderRadius: a/2.0,
        marginBottom:40
    },
    self: {
        width: a,
        height: a,
        backgroundColor: "#573976",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#9291C2",
        borderWidth: 4,
        borderRadius: a/2.0,
    },
    itemImage: {
        width: 40,
        height: 40,
        marginBottom: 0
    },
    text:{
        marginTop: 5,
        fontSize: 12,
        color:"#fff"
    },
    spheres: {
        width: a,
        height: a,
        borderColor: "#9291C2",
        borderWidth: 4,
        borderRadius: a/2.0,
        marginBottom:40,
        alignItems:'center'
    },
    downloads: {
        width: a,
        height: a,
        backgroundColor: "#573976",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#9291C2",
        borderWidth: 4,
        borderRadius: a/2.0,
    },
    avatar:{
        flex:1
    }
});
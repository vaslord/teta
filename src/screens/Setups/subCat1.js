import React from 'react';
import { Text,StyleSheet,ScrollView } from 'react-native';
import FooterButton from "../../components/FooterButton";
import {Localization,LinearGradient} from 'expo'
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'
export default class subCat1 extends React.Component {
    state = {
        isVisible:false
    };
    onPress = (text) => {
        this.props.navigation.navigate('BeforeVideoModal',{
            key:1,
            cat: text
        });
    };
    render() {
        const text = this.props.navigation.getParam('cat','проблема');
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1}}
            >
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState({isVisible:false})} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
                <ScrollView style={styles.msg}>
                    <Text style={[styles.text,{marginBottom:10}]}>{Localization.locale==="ru-RU"?("Проверьте, пожалуйста, с помощью теста убеждение:"):("Check with the conviction test, please: ")}</Text>
                    <Text style={styles.text}>{Localization.locale==="ru-RU"?("У меня есть "+text):("I have "+text)}</Text>
                </ScrollView>
                <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                <FooterButton
                    text={Localization.locale==="ru-RU"?"Продолжить":"Continue"}
                    onPress={()=>this.onPress(text)}
                />
            </LinearGradient>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1,
    },
    text:{
        textAlign:"center",
        color:"#fff",
        fontSize:20
    },
    image:{
        alignItems:'center',
        marginBottom:20,
        width: 100,
        height: 90
    },
    button:{
        height:50,
        justifyContent:"center",
        backgroundColor:'#a94eef',
        width:"100%"
    },
    msg:{
        justifyContent:'center',
        paddingHorizontal:40
    }
})



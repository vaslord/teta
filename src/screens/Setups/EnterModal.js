import React from 'react';
import { Text,TextInput,StyleSheet,ScrollView,AsyncStorage,View} from 'react-native';
import FooterButton from '../../components/FooterButton'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import i18n from 'i18n-js'
import {Localization,LinearGradient} from 'expo'
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'
export default class EnterModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isLoading:true,isVisible:false, key:this.props.navigation.getParam('key','1') };
        this._loadAsync();
    }
    _loadAsync= async ()=>{
        const t1 = await AsyncStorage.getItem('subcat1');
        let texts = this.getText(t1.toLowerCase());
        await this.setState({
            isVisible:false,
            isLoading: false,
            subCat: t1,
            text:t1+texts.question,
            texts:texts
        });
    };
    texts= {};
    getText=(t1)=>{
        const ru ={
            title:"Как вам мешает "+t1+'?',
            question:' мешает мне ',
            placeholder:"Ответьте пожалуйста на вопрос, например, «Заставляет чувствовать себя глупой» ",
        };
        const en = {
            title: "How does bother you "+t1+'?',
            question:' bothers me ',
            placeholder: "Please answer the question, for example, \"It makes me feel like a fool\"",
        };
        const ru2={
            title:'Как вам помогает '+t1+'?',
            question:' помогает мне ',
            placeholder:"Например, «Помогает мне становиться лучше», ",
        };
        const en2= {
            title: "How does "+t1+" help you?",
            question:' helps me ',
            placeholder: "For example, \"It helps me to get better\",",
        };
        if (this.state.key===1) return (Localization.locale==="ru-RU")? ru:en;
        else return (Localization.locale==="ru-RU")? ru2:en2;
    };




    onPress = async () =>{
        if (this.state.key===2) await AsyncStorage.setItem('form2',this.state.text?(this.state.text.charAt(0).toLowerCase()+this.state.text.substring(1)):"нет ответа");
        await this.props.navigation.push('subCat3',{
            key: this.state.key,
            cat:this.state.text,
            subCat:this.state.subCat
        });
    };
    render() {
        if(!this.state.isLoading)
            return (
                <LinearGradient
                    colors={['#5B2453','#252285']}
                    style={{flex:1}}
                >
                    <ScrollView style={styles.msg}>
                        <Text style={{fontSize:16,textAlign:'center',color:"#fff"}}>{this.state.texts.title}</Text>
                        <TextInput
                            style={{ backgroundColor: 'transparent',paddingHorizontal:10,color:'#fff',marginTop:20,fontSize:16,textAlign:"center",}}
                            onChangeText={(text) => this.setState(prevState=>({
                                ...prevState,
                                text:text
                            })
                                )}
                            value={this.state.text}
                            placeholder={this.state.text}
                            placeholderTextColor = "#fff"
                            multiline
                            autoFocus
                            blurOnSubmit
                        />
                    </ScrollView>
                    <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                    <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
                    <FooterButton
                        disabled = {this.state.text?false:true}
                        onPress={this.onPress}
                        text={i18n.t('continue')}
                    />
                </LinearGradient>
            );
        else return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1}}
            />
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1,
    },
    text:{
        fontSize: 16,
        textAlign:"center",
    },
    msg:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    }
});



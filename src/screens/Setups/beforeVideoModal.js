import React from 'react';
import { View,Text,Image,StyleSheet,ScrollView } from 'react-native';
import FooterButton from "../../components/FooterButton";
import {Localization,LinearGradient} from 'expo'
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'
export default class BeforeVideoModal extends React.Component {
    state = {
        isVisible:false
    };
    onPress = () => {
        this.props.navigation.navigate('VideoModal',{
            key: this.props.navigation.getParam('key','1'),
            cat: this.props.navigation.getParam('cat','problem')
            }
        )
    };
    render() {
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1}}
            >

                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState({isVisible:false})} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
                <ScrollView style={styles.msg}>
                    <View style={{alignItems:'center',marginBottom:20}}>
                        <Image style={styles.image} source={require('../../assets/images/literature.png')} resizeMode={'contain'}/>
                    </View>
                    <Text style={[styles.text,{fontSize:24}]}>{Localization.locale==="ru-RU"?"Сейчас мы вам покажем, как провести тестирование убеждений":"Now we will show you how to test beliefs"}</Text>
                </ScrollView>
                <CloseIcon onPress={()=> this.setState({isVisible:true})}/>
                <FooterButton
                    text={Localization.locale==="ru-RU"?"Продолжить":"Continue"}
                    onPress={this.onPress}
                />
            </LinearGradient>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1,
    },
    text:{
        textAlign:"center",
        color:"#fff",
        padding:40
    },
    image:{
        alignItems:'center',
        marginBottom:20,
        width: 100,
        height: 90
    },
    button:{
        height:50,
        justifyContent:"center",
        backgroundColor:'#a94eef',
        width:"100%"
    },
    msg:{
        justifyContent:'center',
    }
})



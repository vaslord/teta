import React from 'react';
import {View,Text,Image,StyleSheet,ScrollView,TouchableOpacity} from 'react-native'
import {Video} from 'expo'
import FooterButton from '../../components/FooterButton'
import { NavigationEvents } from 'react-navigation';
import i18n from "i18n-js"
import { Localization,LinearGradient } from 'expo';

export default class Setups extends React.Component {
      static navigationOptions = {
          title: 'ThetaSelf',
          headerLeft:(<View/>),
          tabBarVisible : false
      };
        state = {
            shouldPlay: true,
        };
        handlePlayAndPause = () => {
            this.setState((prevState) => ({
                shouldPlay: !prevState.shouldPlay
            }));
        };
      _continue = () => {
            this.setState({shouldPlay:false})
            this.props.navigation.navigate('Spheres')
      };
    ru={
        title:"Как работает этот метод",

    };
    en={
        title: "How this method works",
    };
    texts = Localization.locale==="ru-RU"? this.ru:this.en;
  render() {
    return(
        <LinearGradient
            colors={['#5B2453','#252285']}
            style={styles.main}
        >
            <NavigationEvents
                onDidBlur={ ()=> this.setState({shouldPlay:false})}
                onDidFocus={()=>this.setState({shouldPlay:true})}
            />
          <ScrollView style={styles.container}>
              <View style={{alignItems:'center'}}>
                  <Image source={require('../../assets/images/literature.png')} style={styles.image}/>
              </View>
              <Text style={styles.text}>{this.texts.title}</Text>
              <TouchableOpacity onPress={this.handlePlayAndPause}>
                  <Video
                      source={{uri:'http://server.thetaself.com/hello.mp4'}}
                      isLooping
                      useNativeControls={true}
                      rate={1.0}
                      volume={1.0}
                      isMuted={false}
                      resizeMode="contain"
                      style={styles.video}
                      shouldPlay={this.state.shouldPlay}
                  />
              </TouchableOpacity>
          </ScrollView>
            <FooterButton
                onPress={this._continue}
                text={i18n.t('continue')}
            />
        </LinearGradient>
    );
  };

}

var styles = StyleSheet.create({
    main:{
        flex:1
    },
    container:{
        alignItems:"center",
        justifyContent:"center"
    },
    text:{
        fontSize: 20,
        textAlign:"center",
        color: "#fff",
        marginTop:20
    },
    image:{
        marginTop:40,
        width: 80,
        height: 80
    },
    video: {
        width: 290,
        height: 170,
        margin: 40
    },
    button:{
        flex:1,
        justifyContent:"flex-end",
        width:"100%"
    },
    button_text:{
        fontSize: 14,
        marginRight: 20,
        color:"#fff"
    }
});

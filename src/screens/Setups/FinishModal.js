import React from 'react';
import {View,Text,Image,StyleSheet,ScrollView} from 'react-native'
import ThetaButton from '../../components/ThetaButton'
import {Localization,LinearGradient} from 'expo'
import {CloseIcon} from "../../components/Icons";
import OutModal from '../../components/OutModal'
export default class FinishModal extends React.Component {
    constructor(props){
        super(props);
        const key = props.navigation.getParam('key','3');
        this.state={
            isVisible:false,
            key:key
        };
        if(key===3){
            this.state.text=[
                "Теперь ваше пространство освободилось для принятия новых установок"
            ]
        }
        else this.state.text=[
            "Теперь Вас ждет новый образ мышления и качество жизни. Встречайте и проявляйте нового Себя осознанно и с радостью! ",
            "Рекомендуем закрепить эти установки, для этого Вы можете проработать их глубже."
            ]
    }
    pressHandler1 = () =>{
        this.props.navigation.navigate('subCat4')
    };
    pressHandler2 = () =>{
        this.props.navigation.navigate('EndModal')
    };
    renderButton = () =>{
        if (this.state.key===3){
            return <ThetaButton
                onPress = {this.pressHandler1}
                width = {265}
                title={Localization.locale==="ru-RU"? 'ПОЛУЧИТЬ НОВЫЕ УСТАНОВКИ': 'GET NEW INSTALLATIONS'}
            />
        }
        else return <ThetaButton
            onPress = {this.pressHandler2}
            width = {265}
            title={Localization.locale==="ru-RU"? 'ПРОРАБОТАТЬ ГЛУБЖЕ': 'PROCESS DOWN'}
        />
    };
    render() {
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={styles.main}
            >
                <Text style={styles.title}>{Localization.locale==="ru-RU"? 'Поздравляем,':'Congratulations,'}</Text>
                <Text style={styles.title}>{Localization.locale==="ru-RU"? 'Вы справились!':'You did it!'}</Text>
                <View style={{alignItems:'center'}}>
                    <Image source={require('../../assets/images/lotus-flower.png')} style={styles.image} resizeMode = 'contain' />
                </View>
                <Text style={styles.text}> {this.state.text[0]}</Text>
                <Text style={styles.text}>{this.state.text[1]}</Text>
                {this.renderButton()}
                <CloseIcon onPress={()=> this.setState(prevState=>({...prevState,isVisible:true}))}/>
                <OutModal isVisible={this.state.isVisible} goBack={()=>this.setState((prevState)=>({...prevState,isVisible:false}))} onYes={()=>{this.props.navigation.navigate('Spheres')}} />
            </LinearGradient>
        );
    }
}


const styles = StyleSheet.create({
    title:{
        textAlign:'center',
        fontWeight:'bold',
        fontSize:24,
        color:'#FFF'
    },
    main:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    image:{
        width:131,
        height:122,
        marginVertical: 20
    },
    text:{
        fontSize: 16,
        textAlign:'center',
        color:'#FFF'
    }
});


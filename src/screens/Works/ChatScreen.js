import React from 'react'
import { GiftedChat,Bubble } from 'react-native-gifted-chat'
import { Header } from 'react-navigation';
import { KeyboardAvoidingView,View } from "react-native";
import i18n from 'i18n-js';
import {LinearGradient} from 'expo'


export default class ChatScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            messages: [],
            isLoading: true
        };
    }
    static navigationOptions = ({ navigation }) => {
        return {
            title: i18n.t('help')
        };
    };
    msgId = 0;
    id = this.props.id;
    base64 = require('base-64');
    token = this.props.token;
    getId(){
        this.msgId+=1;
        return this.msgId
    }
    componentDidMount (){
        this.getMessages();
        setInterval(this.getMessages,2000)
    }

    getMessages = () =>{
        return fetch(
            'http://app.thetaself.com/chats/get?id='+this.id, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Basic '+ this.base64.encode(this.token+":")
                }
            }
        )
            .then((response) => response.json())
            .then((responseJson) => {
                for(i =0;i<responseJson.length;i++){
                    responseJson[i]._id = this.getId()
                    responseJson[i].user = {
                        _id : responseJson[i].isClient ? 1 : 2
                    }
                }
                this.setState({
                    isLoading: false,
                    messages: responseJson,
                }, function(){

                });
            })
            .catch((error) =>{

            });
    }


    onSend(messages = []) {
        this.setState( prevState => ( {
            messages: messages.concat(prevState.messages)
        }));
        fetch(
            `http://app.thetaself.com/chats/send?text=${messages[0].text}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Authorization': 'Basic '+ this.base64.encode(this.token+":")
                }
            }
        )
            .catch((error) =>{

            });
    }
    renderBubble (props) {
        return (
            <Bubble
                {...props}
                wrapperStyle={{
                    right: {
                        backgroundColor: "#fff"
                    }
                }}
                textStyle={{
                    right: {
                        color:"#000"
                    }
                }}
            />
        )
    }
    render() {
        if(!this.state.isLoading)
            return (
                <LinearGradient style={{flex:1}}
                                colors={['#5B2453','#252285']}
                                start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
                >
                    <KeyboardAvoidingView  style = {{flex:1}} behavior='padding' enabled keyboardVerticalOffset = {Header.HEIGHT + 20}>
                        <GiftedChat
                            placeholder={i18n.t('typeMsg')}
                            renderAvatar={null}
                            messageIdGenerator = {()=>this.getId()}
                            messages={this.state.messages}
                            onSend={messages=>this.onSend(messages)}
                            renderBubble={this.renderBubble}
                            user={{
                                _id: 1
                            }}
                        />
                    </KeyboardAvoidingView>
                </LinearGradient>
            );
        else return <LinearGradient style={{flex:1}}
                                    colors={['#5B2453','#252285']}
                                    start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
        />
    }
}
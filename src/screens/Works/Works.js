import React from 'react';
import { StyleSheet,View,WebView,Modal,Text,AsyncStorage } from 'react-native';
import {ButtonGroup} from 'react-native-elements';
import { Localization,LinearGradient } from 'expo';
import ThetaButton from '../../components/ThetaButton'
import {CloseIcon} from "../../components/Icons";
import ChatScreen from './ChatScreen'

export default class Works extends React.Component {
    constructor(){
        super();
        this.state={
            isLoading: true,
            isSub: false,
            isPaying:false,
            selectedIndex: 2,
            payUrl:'http://app.thetaself.com/error',
            getUrl: false
        };
        this._loadAsync();
    }
  static navigationOptions = {
      title: Localization.locale==="ru-RU"? "Проработки":"Workout",
      headerRight:''
  };
  _loadAsync=async()=>{
      const user = JSON.parse(await AsyncStorage.getItem('User'));
      await this.setState(prevState=>({
          ...prevState,
          isLoading:false,
          isSub:user.isSub,
          user:user,
          token:user.access_token
      }))
  };
  base64 = require('base-64');
  worksRu = require("../../Texts/myWorks").works;
  worksEn = require("../../TextsEn/myWorks").works;
  works = Localization.locale==="ru-RU"? this.worksRu:this.worksEn;
  ru={
      title:"Приобретите нашу подписку и получите доступ к персональным установкам!",
      pay:"Оплата картой",
      paySber:"Оплата Сбербанк онлайн",
      week:"1 неделя",
      month:"1 месяц",
      months3:"3 месяца",
      year:"1 год",
      weekInfo:"Подписка на 1 неделю",
      monthInfo:"Подписка на 1 месяц",
      monthsInfo:"Подписка на 3 месяца",
      yearInfo:"Подписка на 1 год"

  };
  en={

      title: "Get our subscription and get access to personal settings!",
      pay: "Card payment",
      paySber:"Sberbank online",
      week:"1 week",
      month:"1 month",
      months3:"3 months",
      year:"1 year",
      weekInfo: "Subscription for 1 week",
      monthInfo: "Subscription for 1 month",
      monthsInfo: "Subscription for 3 months",
      yearInfo:"Subscription for 1 year"
  };
  texts = Localization.locale==="ru-RU"? this.ru:this.en;
  getUrl = async ()=>
      {
          try {
              let response = await fetch(
                  `http://app.thetaself.com/payments/pay?type=${this.state.selectedIndex}`, {
                      method: 'GET',
                      headers: {
                          Accept:'application/json',
                          Authorization: 'Basic '+ this.base64.encode(this.state.token+":")
                      }
                  }
              );
              await this.setState((prevState)=> ({
                      ...prevState,
                      payUrl: response._bodyText,
                      getUrl: true
                  })
              );
          } catch (error) {
          }
      };
    getUrlSber = async ()=>
    {
        try {
            let response = await fetch(
                'http://app.thetaself.com/payments/pay-sberbank', {
                    method: 'GET',
                    headers: {
                        Accept:'application/json',
                        Authorization: 'Basic '+ this.base64.encode(this.state.token+":")
                    }
                }
            );
            await this.setState((prevState)=> ({
                    ...prevState,
                    payUrl: response._bodyText,
                    getUrl: true
                })
            )
        } catch (error) {
        }
    };
    _Pay = () =>{
        this.setState(prevState=>({
            ...prevState,
            isPaying:true
        }));
        this.getUrl()
    };
    _PaySberbank = () =>{
    };
  _onModalClose=()=>{
      this.setState((prevState)=>{
          return {
              ...prevState,
              isSub:true,
              isPaying:false
          }
      })
  };
  updateIndex = (selectedIndex)=> {
      this.setState((prevstate)=>{
          return {
              ...prevstate,
              selectedIndex: selectedIndex
          }}
          )
    };
  week = () => <View style={[styles.subContainer]}>
          <Text style={styles.subTitle}>{this.texts.week}</Text>
          <Text style={styles.subInfo}>{this.texts.weekInfo}</Text>
          <Text style={styles.subPrice}>2$</Text>
  </View>;
  month = () => <View style={[styles.subContainer]}>
        <Text style={styles.subTitle}>{this.texts.month}</Text>
        <Text style={styles.subInfo}>{this.texts.monthInfo}</Text>
      <Text style={styles.subPrice}>6$</Text>
  </View>;
  year = () => <View style={[styles.subContainer]}>
          <Text style={styles.subTitle}>{this.texts.months3}</Text>
          <Text style={styles.subInfo}>{this.texts.monthsInfo}</Text>
      <Text style={styles.subPrice}>15$</Text>
      </View>;
    year1 = () => <View style={[styles.subContainer]}>
        <Text style={styles.subTitle}>{this.texts.year}</Text>
        <Text style={styles.subInfo}>{this.texts.yearInfo}</Text>
        <Text style={styles.subPrice}>50$</Text>
    </View>;
  render() {
      const buttons = [{element:this.week}, {element:this.month}, {element:this.year},{element:this.year1}]
      if(!this.state.isLoading)
          if(this.state.isSub)
                return (
                    <ChatScreen
                        id={this.state.user.id}
                        token={this.state.user.access_token}
                    />
                );
        else
            return <LinearGradient style={{flex:1,padding:10}}
                                  colors={['#5B2453','#252285']}
                                  start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <CloseIcon onPress={()=> this.props.navigation.goBack()} />
                <View style={styles.notSub}>
                <Text style={{textAlign:'center',fontSize:17,fontWeight:'bold',color:"#fff",marginHorizontal:20,marginBottom:20}}>
                    {this.texts.title}
                </Text>
                <ButtonGroup
                    buttons={buttons}
                    onPress={this.updateIndex}
                    selectedButtonStyle = {{backgroundColor:"#8473A3"}}
                    selectedIndex={this.state.selectedIndex}
                    containerStyle={styles.subscriptions}
                    buttonStyle={{}}
                />
                <ThetaButton
                    onPress={this._Pay}
                    title={this.texts.pay}
                    width={260}
                />
                <ThetaButton
                    onPress={this._PaySberbank}
                    width={260}
                    title={this.texts.paySber}
                    disabled={true}
                />
                <Modal visible = {this.state.isPaying && this.state.getUrl} animationType="slide"  onRequestClose={this._onModalClose}
                >
                    <WebView
                        source={{uri:this.state.payUrl}}
                    />
                </Modal>
            </View>
            </LinearGradient>;
      else return <LinearGradient style={{flex:1,padding:10}}
                                  colors={['#5B2453','#252285']}
                                  start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
      />
  }
}

const styles = StyleSheet.create({
  container: {
        flex: 1,
        paddingTop: 15,
  },
    notSub:{
      paddingTop: 40,
      flex:1,
      alignItems:'center',
        justifyContent:'center'
    },
    button:{
        width: 200,
        height:60,
        fontSize:14,
        borderRadius:20,
        margin: 5,
    },
    subscriptions:{
        height:200,
        borderRadius:15,
        justifyContent:'center',
        margin:10,
        marginBottom:30,
        backgroundColor:"#5F4E8F"
    },
    subContainer:{
        flex:1,
        justifyContent:'center'
    },
    subTitle:{
        fontSize:14,
        textAlign:'center',
        color:'#fff'
    },
    subInfo:{
        fontSize:10,
        textAlign:'center',
        color:'#fff'
    },
    subPrice:{
        fontSize:20,
        textAlign:'center',
        fontWeight:'bold',
        color:'#fff'
    }
});

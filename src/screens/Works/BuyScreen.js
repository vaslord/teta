import React from 'react';
import { Text,StyleSheet } from 'react-native';
import ThetaButton from "../../components/ThetaButton";
import {CloseIcon} from "../../components/Icons";
import {Localization,LinearGradient,Icon} from 'expo'

export default class BuyScreen extends React.Component {
    static navigationOptions = {
        title: Localization.locale==="ru-RU"? "Мои проработки":"Working Through",
        headerRight:''
    };
    textsRu = {
        title : "Данная функция доступна только по премиальной подписке",
        subTitle: "В подписку входят:",
        button:"ПОЛУЧИТЬ ПРЕМИУМ-ДОСТУП",
        list : [
            "Глубокая проработка установки",
            "Помощь тета-хилера онлайн",
            "Медитация «Манифестация»",
            "Медитация «Практика принятия себя»",
            "Медитация прощения",
            "Тета-медитация «Единство»"
        ]
    };
    textsEn = {
        title: "This feature is only available through premium subscription",
        subTitle: "Subscription includes:",
        button: "GET PREMIUM ACCESS",
        list: [
            "Deep installation study",
            "Help theta healer online",
            "Meditation 'Manifestation'",
            "Meditation 'Practice of taking yourself'",
            "Meditation of forgiveness",
            "Theta Meditation 'Unity'"
        ]
    };
    texts = Localization.locale==="ru-RU"?this.textsRu:this.textsEn;
    onPress = () => {
        this.props.navigation.navigate('Works');
    };
    render() {
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1,justifyContent:'center',padding:20}}
            >
                <CloseIcon onPress={()=> this.props.navigation.goBack()} />
                <Text style={styles.title}>{this.texts.title}</Text>
                <Text style={styles.subTitle}>{this.texts.subTitle}</Text>
                <Text style={styles.item}>{this.texts.list[0]}</Text>
                <Text style={styles.item}>{this.texts.list[1]}</Text>
                <Text style={styles.item}>{this.texts.list[2]}</Text>
                <Text style={styles.item}>{this.texts.list[3]}</Text>
                <Text style={styles.item}>{this.texts.list[4]}</Text>
                <Text style={[styles.item,{marginBottom:20}]}>{this.texts.list[5]}</Text>
                <ThetaButton
                    title={this.texts.button}
                    onPress={this.onPress}
                    width={260}
                />
            </LinearGradient>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex: 1,
        marginBottom:20
    },
    title:{
        color:'#fff',
        fontSize:16,
        textAlign:'center'
    },
    subTitle:{
        marginTop:20,
        marginBottom:10,
        color:'#fff',
        fontSize:16,
        textAlign:'center'
    },
    item:{
        color:'#fff',
        fontSize:16,
        textAlign:'center',
    }

});



import React from 'react';
import { View,Text,ScrollView,StyleSheet } from 'react-native';
import {Button} from 'react-native-elements'
import { LinearGradient,Localization } from 'expo';

export default class HealerModal extends React.Component {

    render() {
        const { navigation } = this.props;
        const contacts = navigation.getParam('contacts', '');
        return (
            <LinearGradient
                colors={['#5B2453','#252285']}
                style={{flex:1}}
            >
                    <ScrollView style={styles.container}>
                        <Text style={{fontSize:24,color:"#fff",fontWeight:'bold'}}>{Localization.locale==="ru-RU"? "Контакты":"Contacts"}</Text>
                        <View style={styles.row}>
                            <Text style={styles.item}>{Localization.locale==="ru-RU"? 'Сайт:':'Site:'}</Text>
                            <Text style={styles.value}>{contacts.Сайт}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.item}>Skype:</Text>
                            <Text style={styles.value}>{contacts.Skype}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.item}>{Localization.locale==="ru-RU"? 'Тел.:':'Phone:'}</Text>
                            <Text style={styles.value}>{contacts.Номер}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.item}>Email</Text>
                            <Text style={styles.value}>{contacts.Email}</Text>
                        </View>
                    </ScrollView>
                    <View
                        style={styles.footer}
                    >
                        <Button
                            onPress={()=>{this.props.navigation.goBack()}}
                            buttonStyle={styles.button}
                            title={Localization.locale==="ru-RU"? "Закрыть":"Close"}
                        />
                    </View>
            </LinearGradient>
        )
    }
}



const styles = StyleSheet.create({
    main:{
        flex:1,
    },
    container:{
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
    footer:{
        height: 52,
        width:'100%',
        justifyContent: 'center',
        alignItems:'flex-end',
        paddingRight:40,
        backgroundColor:"#514E9D"
    },
    button:{
        backgroundColor: 'Transparent',
        outline:'none',
        fontSize:14,
        border: 'none'
    },
    row:{
        flexDirection: 'row',
        fontSize: 14,
    },
    item:{
        color: '#9291C2'
    },
    value:{
        color:'#fff',
        marginLeft: 10,
    }
})


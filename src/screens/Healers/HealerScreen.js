import React from 'react'
import {View,Text,Image,StyleSheet,ScrollView} from 'react-native'
import ThetaButton from '../../components/ThetaButton'
import {Localization,LinearGradient} from 'expo'
export default class HealerScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'Врач'),
        };
    };
    _onPress = (info) => {
        this.props.navigation.push('HealerContacts',{
            contacts:info,
        })
    };
    render() {
        const { navigation } = this.props;
        const info = navigation.getParam('info', '');
        if (info){
            return (
                <LinearGradient style={{flex:1,padding:10}}
                                colors={['#5B2453','#252285']}
                                start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
                >
                    <View style={styles.main}>
                        <View style={styles.container}>
                            <Image source={require('../../assets/images/healerFull.png')} style={styles.image} resizeMode = 'cover' />
                            <Text style={styles.name}>{info.Фамилия+' '+info.Имя+' '+info.Отчество}</Text>
                            <Text style={styles.city}>{info.Город}</Text>
                            <Text style={styles.details}>{info.Подробнее}</Text>
                        </View>
                        <ThetaButton
                            onPress = {() => {this._onPress(info)}}
                            title={Localization.locale==="ru-RU"? 'ПОКАЗАТЬ КОНТАКТЫ':"SHOW CONTACTS"}
                        />
                    </View>
                </LinearGradient>


            );
        }
        else return  <LinearGradient style={{flex:1,padding:10}}
                                     colors={['#5B2453','#252285']}
                                     start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
        />

    }
}

const styles = StyleSheet.create({
    main:{
        flex:1,
    },
    container:{
        marginBottom: 20
    },
    image:{
        height:240,
        width:'100%',
        marginBottom: 20
    },
    city: {
        marginBottom: 20,
        fontSize: 14,
        textAlign:'center',
        color:"#fff"
    },
    name:{
        fontSize: 16,
        fontWeight: 'bold',
        textAlign:'center',
        marginBottom: 20,
        color:"#fff"
    },
    details:{
        fontSize: 14,
        textAlign:'left',
        marginHorizontal: 20,
        color:"#fff"
    }

})
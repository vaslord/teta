import React, { Component } from "react";
import {StyleSheet, FlatList,AsyncStorage} from "react-native";
import ListHealer from '../../components/ListHealer';
import {Localization} from "expo";
import { LinearGradient } from 'expo';


export default class HomeScreen extends Component {
    constructor(){
        super();
        this.state ={ isLoading: true};
        this.componentDidMount();
    }
    static navigationOptions = {
        title: Localization.locale==="ru-RU"? "Тета-хилеры":"Theta-healers",
        headerRight:''
    };

    modalOpenHandler = (info) => {
        this.props.navigation.push('HealerInfo',{
            info:info,
            title:info.Имя
        })
    };

    componentDidMount = async () =>{
        const userToken = await AsyncStorage.getItem('userToken');
        const base64 = require('base-64');
        await fetch(
            'http://app.thetaself.com/doctors', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Basic '+ base64.encode(userToken+":")
                }
            }
        )
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    healers: responseJson,
                });
            })
            .catch((error) =>{
            });
    };
    render() {
        if(!this.state.isLoading){
            return (
                <LinearGradient style={{flex:1,padding:10}}
                    colors={['#5B2453','#252285']}
                    start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
                >
                    <FlatList
                        style={styles.container}
                        data = {this.state.healers}
                        renderItem={(info) => {
                            return <ListHealer
                                name={info.item.Фамилия+' '+info.item.Имя+' '+info.item.Отчество}
                                itemImage={require('../../assets/images/healer.png')}
                                isAble = {true}
                                time = {info.item.ЧасыПриема}
                                city= {info.item.Город}
                                onItemPressed={()=>this.modalOpenHandler(info.item)}
                            />
                        }
                        }
                    />
                </LinearGradient>
            );}
        else return <LinearGradient style={{flex:1}}
            colors={['#5B2453','#252285']}
            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
        />
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
    },
});
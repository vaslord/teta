import React from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
    Image,Text,
    TouchableOpacity,
    Linking
} from 'react-native';
import MainTabNavigator from '../navigation/MainTabNavigator';
import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
import { LinearGradient } from 'expo';
import AppIntroSlider from 'react-native-app-intro-slider';
import {Button,Input, FormLabel, FormInput, FormValidationMessage} from 'react-native-elements'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Icon} from 'expo'
import DateTimePicker from 'react-native-modal-datetime-picker';
import RNPickerSelect from 'react-native-picker-select';
import { Localization } from 'expo';
import i18n from 'i18n-js'
function number_validator(number){
    num = number.replace(/[^\d]/g,'');
    if(num.length!=11) return {
        valid:false,
        error:"Номер должен содержать 11 цифр"
    };
    else if ((num[0]!=='7')&&(num[0]!=='8')) return {
        valid:false,
        error:"Номер должен начинаться с 7 или 8"
    };
    else return {
        valid:true,
        number:'7'+num.substring(1)
    }
}

const styles = StyleSheet.create({
    mainContent: {
        flex: 1,
        alignItems: 'center',
    },
    Content:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding:5

    },
    logoImage:{
        marginTop:60,
        width:153,
        height:115
    },
    image: {
        height: 177,
        marginTop:40,
        backgroundColor: 'transparent'
    },
    text: {
        color: '#fff',
        backgroundColor: 'transparent',
        textAlign: 'center',
    },
    title: {
        fontSize: 17,
        color: 'white',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginTop: 30,
    },
    btnContainer:{
        backgroundColor:'#fff',
        alignItems:'center',
        borderRadius:25,
        marginBottom: 40,
    },
    button: {
        backgroundColor:'#fff',
        borderRadius:25,
        width: 163,
        height: 40,
    },
    btnText:{
        fontSize:17,
        color:'#252285',
        textAlign:'center'
    }
});
const styles2 = StyleSheet.create({
    mainContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnContainer:{
        backgroundColor:'#fff',
        alignItems:'center',
        borderRadius:25,
        marginTop: 20,
    },
    button: {
        alignItems:'center',
        backgroundColor:'#fff',
        borderRadius:25,
        width:wp('70%'),
        height:40,
    },
    btnText:{
        fontSize:17,
        color :'#252285',
        textAlign: 'center'
    },
    label:{
        marginBottom:20,
        textAlign:'center',
        color:'#fff',
        fontSize:16
    },
    input:{
        width:wp('70%'),
        height:40,
        backgroundColor:"#573578",
        borderRadius:20,
        fontSize:17,
        alignItems:'center',
        borderBottomWidth: 0,
        marginBottom:10
    },
    image:{
        marginBottom:30,
        width:wp('12,5%'),
        height:wp('12,5%')
    },
    icon:{
        position:'absolute',
        top:30,
        left:10
    },
    underInput:{
        width:wp('60%'),
        marginBottom:20,
        textAlign:'center',
        color:'#fff',
        fontSize:12
    }

});
const styles3 = StyleSheet.create({
    mainContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnContainer:{
        backgroundColor:'#fff',
        alignItems:'center',
        borderRadius:25,
        marginTop: 20,
    },
    button: {
        alignItems:'center',
        backgroundColor:'#fff',
        borderRadius:25,
        width:wp('70%'),
        height:40,
    },
    btnText:{
        fontSize:17,
        color :'#252285',
        textAlign: 'center'
    },
    label:{
        width:wp('40%'),
        marginBottom:20,
        textAlign:'center',
        color:'#fff',
        fontSize:12
    },
    title:{
        marginBottom:20,
        textAlign:'center',
        color:'#fff',
        fontSize:20
    },
    input:{
        width:wp('70%'),
        height:40,
        backgroundColor:"#573578",
        borderRadius:20,
        fontSize:17,
        alignItems:'center',
        borderBottomWidth: 0,
        marginBottom:10
    },
    image:{
        marginBottom:30,
        width:wp('12,5%'),
        height:wp('12,5%')
    },
    icon:{
        position:'absolute',
        top:30,
        left:10
    },
    underInput:{
        width:wp('60%'),
        textAlign:'center',
        color:'#fff',
        fontSize:12
    },
    pickerContainer:{
        width:wp('70%'),
        height:40,
        backgroundColor:"#573578",
        borderRadius:20,
        alignItems:'center',
        justifyContent:'center',
        borderBottomWidth: 0,
        marginBottom:10,
        fontSize:17,
        textAlign:'center',
        color: "#fff"
    },
    picker :{
        fontSize:17,
        textAlign:'center',
        color: "#fff"
    },
});
const slidesRu = [
    {
        key: 'somethun',
        title: 'Вас ждет новый образ мышления и качество жизни. Встречайте и проявляйте нового Себя осознанно и с радостью!',
        image: require('../assets/images/dna.png'),
        imageStyle: styles.image,
        colors: ['#5B2453','#252285']
    },
    {
        key: 'somethun1',
        title: 'ThetaSelf позволяет не только исцеляться, но и воздействовать на психику, убирая из неё противоречия',
        image: require('../assets/images/structure.png'),
        imageStyle: styles.image,
        colors: ['#5B2453','#252285'],
    },
    {
        key: 'somethun2',
        title: 'ThetaSelf позволяет не только исцеляться, но и воздействовать на психику, убирая из неё противоречия',
        image: require('../assets/images/atom.png'),
        imageStyle: styles.image,
        colors: ['#5B2453','#252285'],
    },
];
const slidesEn = [
    {
        key: 'somethun',
        title: 'ThetaSelf allows not only to heal, but also to influence the psyche, removing contradictions from it',
        image: require('../assets/images/dna.png'),
        imageStyle: styles.image,
        colors: ['#5B2453','#252285'],
    },
    {
        key: 'somethun1',
        title: 'ThetaSelf allows not only to be healed, but also to influence the psyche, removing contradictions from it',
        image: require('../assets/images/structure.png'),
        imageStyle: styles.image,
        colors: ['#5B2453','#252285'],
    },
    {
        key: 'somethun2',
        title: 'ThetaSelf allows not only to be healed, but also to influence the psyche, removing contradictions from it',
        image: require('../assets/images/atom.png'),
        imageStyle: styles.image,
        colors: ['#5B2453','#252285'],
    },
];
slides = Localization.locale==="ru-RU"?slidesRu:slidesEn
class StartScreen extends React.Component {

    _signInAsync = async () => {
        this.props.navigation.navigate('Number');
    };
    _renderItem = props => (
        <View style={[styles.Content, {
            paddingTop: props.topSpacer,
            paddingBottom: props.bottomSpacer,
            width: props.width,
            height: props.height,
        }]}>
            <Image source={props.image} style={props.imageStyle} resizeMode='contain'/>
            <Text style={styles.title}>{props.title}</Text>
        </View>
    );
    render() {
        return (
            <LinearGradient style={styles.mainContent}
                colors={['#5B2453','#252285']}
                start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <Image source={require('../assets/images/LoadLogo.png')} style={styles.logoImage} resizeMode='contain'/>
                <AppIntroSlider
                    slides={slides}
                    renderItem={this._renderItem}
                    hideNextButton
                    hideDoneButton
                />
                <Button
                    title={Localization.locale==="ru-RU"?"Начать":"Start"}
                    onPress={this._signInAsync}
                    buttonStyle={styles.button}
                    titleStyle={styles.btnText}
                    containerStyle={styles.btnContainer}
                />
            </LinearGradient>
        );
    }
}

class AuthLoadingScreen extends React.Component {
    constructor() {
        super();
        this._bootstrapAsync();
    }

    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem('userToken');
        const User = await fetch(
            `http://app.thetaself.com/users/auth?token=${userToken}`, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json'
                }
            }
        )
            .then((response) => response.json())
            .catch((error) =>{
            });
        try {
            await AsyncStorage.setItem('userToken', User.access_token?User.access_token:'');
            await AsyncStorage.setItem('User',JSON.stringify(User));
        }
        catch (error){
        }
        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.

        await this.props.navigation.navigate(User ? 'App' : 'Auth');

    };

    // Render any loading content that you like here
    render() {
        return (
            <LinearGradient style={{flex:1,paddingLeft:13}}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <ActivityIndicator />
                <StatusBar barStyle="default" />
            </LinearGradient>
        );
    }
}

class NumberScreen extends React.Component {
    state={
        isValid: false,
        number:'',
        error:''
    };
    _validate=()=>{
        let num = number_validator(this.state.number);
        if (num.valid) this._signInAsync(num.number);
        else this.setState(prevState=>({
            ...prevState,
            error:num.error
        }))
    };
    _signInAsync = async (number) => {
        fetch(
            `http://app.thetaself.com/users/login?phone=${encodeURIComponent(number)}`,{
                method: 'GET',
                headers: {
                    'Accept': 'application/json'
                }
            }
        )
            .then(response => response.json())
            .then((responsejson)=>{
                if (responsejson.message) this.setState(prevState=>({
                    ...prevState,
                    error:responsejson.message
                }));
                else this.props.navigation.navigate('Sms',{
                    number:number
                });
                }
            )
            .catch((error)=>{
        });
    };
    formRu= {
        act: "Войдите в свой аккаунт",
        text:"Номер телефона",
        button: "Войти",
        reg:"Регистрация"
    };

    formEn={
        act: "Log into your account",
        text: "Phone Number",
        button: "Login",
        reg:"Sign in"
    };
    form = Localization.locale==="ru-RU"? this.formRu:this.formEn;
    render() {
        return (
            <LinearGradient style={styles2.mainContent}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <TouchableOpacity style={styles2.icon}  onPress={()=> {this.props.navigation.goBack()}}>
                    <Icon.Ionicons
                        name='ios-arrow-back'
                        size={35}
                        color='#fff'
                    />
                </TouchableOpacity>
                <Image source={require('../assets/images/account-icon.png')} resizeMode='contain' style={styles2.image}/>
                <Input
                    value={this.state.number}
                    onChangeText={(text) => this.setState({number:text})}
                    containerStyle={{alignItems:'center'}}
                    label={this.form.act}
                    labelStyle={styles2.label}
                    placeholder={this.form.text}
                    placeholderTextColor='#fff'
                    inputContainerStyle={styles2.input}
                    inputStyle={{color:'#fff',textAlign:'center'}}
                    keyboardType='numeric'
                    errorMessage={this.state.error}
                />
                <Button
                    title={this.form.button}
                    onPress={this._validate}
                    buttonStyle={styles2.button}
                    titleStyle={styles2.btnText}
                    containerStyle={styles2.btnContainer}
                />
                <Button
                    title={this.form.reg}
                    onPress={()=>this.props.navigation.navigate('Register')}
                    buttonStyle={styles2.button}
                    titleStyle={styles2.btnText}
                    containerStyle={styles2.btnContainer}
                />
            </LinearGradient>
        );
    }
}

class SmsScreen extends React.Component {
    constructor(props){
        super(props);
        this.state={
            otp:'',
            error:''
        };
        this.state.number = props.navigation.getParam('number','')
    }

    _signInAsync = async () => {
        const newToken = await fetch(
            `http://app.thetaself.com/users/check?phone=${encodeURIComponent(this.state.number)}&code=${encodeURIComponent(this.state.otp)}`, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json'
                }
            }
        )
            .then(response => response.json())
            .catch((error) => {
            });
        if (newToken.message) this.setState(prevState => ({
            ...prevState,
            error: newToken.message
        }));
        else {
            await AsyncStorage.setItem('userToken', JSON.stringify(newToken));
            this.props.navigation.navigate('App')
        }
    };
    formRu= {
        act: "Активация",
        code: "Код из СМС",
        text:"Введите сюда код из СМС," +
        "                    отправленного на ваш номер телефона",
        button: "Проверить"
    };

    formEn={

        act: "Activation",
        code: "Code from SMS",
        text: "Enter the code from SMS here," +
        "sent to your phone number",
        button: "Check"
    };
    form = Localization.locale==="ru-RU"? this.formRu:this.formEn;
    render() {
        return (
            <LinearGradient style={styles2.mainContent}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <TouchableOpacity style={styles2.icon}  onPress={()=> {this.props.navigation.goBack()}}>
                    <Icon.Ionicons
                        name='ios-arrow-back'
                        size={35}
                        color='#fff'
                    />
                </TouchableOpacity>
                <Image source={require('../assets/images/account-icon.png')} resizeMode='contain' style={styles2.image}/>
                <Input
                    value={this.state.otp}
                    onChangeText={(text) => this.setState(prevState=>({...prevState,otp:text}))}
                    containerStyle={{alignItems:'center'}}
                    label={this.form.act}
                    labelStyle={styles2.label}
                    placeholder={this.form.code}
                    placeholderTextColor='#fff'
                    inputContainerStyle={styles2.input}
                    inputStyle={{color:'#fff',textAlign:'center'}}
                    keyboardType='numeric'
                    errorMessage={this.state.error}
                />
                <Text style={styles2.underInput}>{this.form.text}</Text>
                <Button
                    title={this.form.button}
                    onPress={this._signInAsync}
                    buttonStyle={styles2.button}
                    titleStyle={styles2.btnText}
                    containerStyle={styles2.btnContainer}
                />
            </LinearGradient>
        );
    }
}

const pickerStyle = StyleSheet.create({
    inputAndroid: {
        alignItems : 'center',
        fontSize: 17,
        color: '#fff',
        textAlign: 'center'
    },
})
const sexesRu = [
    {
        label: 'Муж.',
        value: 'муж',
        color: '#000'
    },
    {
        label: 'Жен.',
        value: 'жен',
        color: '#000'
    },

];
const sexesEn = [
    {
        label: 'Male',
        value: 'муж',
        color: '#000'
    },
    {
        label: 'Female',
        value: 'жен',
        color: '#000'
    },

];
sexes = Localization.locale==="ru-RU"?sexesRu:sexesEn;
class RegScreen extends React.Component {
    state = {
        isDateTimePickerVisible: false,
        client:{
            Фамилия: '',
            Имя: '',
            Отчество:'',
            ДатаРождения:Localization.locale==="ru-RU"?"Дата Рождения":"Birthday",
            Пол:'',
            phone:''
        },
        errors:{}
    };
    _showDateTimePicker = () => this.setState(
        {
            isDateTimePickerVisible: true
        });

    _hideDateTimePicker = () => {
        this.setState(
        {
            isDateTimePickerVisible: false
        })};


    _handleDatePicked = (date) => {
        this.setState( prevState =>({
            client:{
                ...prevState.client,
                ДатаРождения:date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()
            }
        }));
        this._hideDateTimePicker();
    };
    _validate = ()=>{
        let errors = {};
        let empty = true;
        for(let key in this.state.client){
            if(this.state.client[key]===''){
                empty = false;
                errors[key] = 'Введите '+ toLowerCase(key);
                if(key==='phone') errors[key] = "Введите номер";
            }
            else if (key==='phone') {
                num = number_validator(this.state.client.phone);
                if (!num.valid) errors.phone = num.error; 
            }
        }
        if (!empty) this.setState(prevState=>({
            ...prevState,
            errors:errors
        }));
        else this._signInAsync();
    };
    _signInAsync = async () => {
        const newToken = await fetch('http://app.thetaself.com/users/sign', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'content-type': 'application/json'
            },
            body: JSON.stringify(
                this.state.client
            )
        })
            .then((response)=>response.json())
            .catch((error) =>{
            });
        if(newToken.phone) this.setState(prevState=>({
            ...prevState,
            errors:{
                phone:'Этот номер уже занят'
            }
        }))
        else {
            await AsyncStorage.setItem('userToken', newToken);
            await AsyncStorage.setItem('User',JSON.stringify(this.state.client));
            await this.props.navigation.navigate('Settings');
        }
        };
    formRu={
        first:"Вы у нас впервые!",
        input:"Заполните форму ниже," +
        "                    чтобы продолжить",
        f:"Фамилия",
        i:"Имя",
        o:"Отчество",
        bd:"Дата Рождения",
        phone:"Номер телефона",
        con:"Выполняя вход, вы принимаете",
        pol:"Политику конфиденциальности",
        finish:"Завершить регистрацию"
    };
    formEn={

        first: "You are with us for the first time!",
        input: "Fill out the form below," +
        "                    to continue",
        f: "Last Name",
        i: "Name",
        o: "Patronymic",
        bd: "Date of Birth",
        phone:"Phone number",
        con: "By entering, you accept,",
        pol: "Privacy Policy",
        finish: "Registration"
    };
    form = Localization.locale==="ru-RU"? this.formRu:this.formEn;
    render() {
        const placeholder = {
            label: Localization.locale==="ru-RU"?'Выберите пол':'Choose gender',
            value: null,
        };
        return (
            <LinearGradient style={styles2.mainContent}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <TouchableOpacity style={styles2.icon}  onPress={()=> {this.props.navigation.goBack()}}>
                    <Icon.Ionicons
                        name='ios-arrow-back'
                        size={35}
                        color='#fff'
                    />
                </TouchableOpacity>
                <Image source={require('../assets/images/account-icon.png')} resizeMode='contain' style={styles2.image}/>
                <Text style={styles3.title}>{this.form.first}</Text>
                <Text style={styles3.label}>{this.form.input}</Text>
                <Input
                    containerStyle={{alignItems:'center'}}
                    placeholder={this.form.i}
                    placeholderTextColor='#fff'
                    inputContainerStyle={styles3.input}
                    inputStyle={{color:'#fff',textAlign:'center'}}
                    onChangeText={(text) => this.setState( prevState =>({
                        client:{
                            ...prevState.client,
                            Имя:text
                        }
                    }))}
                    errorMessage={this.state.errors.Имя}
                />
                <Input
                    containerStyle={{alignItems:'center'}}
                    placeholder={this.form.f}
                    placeholderTextColor='#fff'
                    inputContainerStyle={styles3.input}
                    inputStyle={{color:'#fff',textAlign:'center'}}
                    onChangeText={(text) => this.setState( prevState =>({
                        client:{
                            ...prevState.client,
                            Фамилия:text
                        }
                    }))}
                    errorMessage={this.state.errors.Фамилия}
                />
                <Input
                    containerStyle={{alignItems:'center'}}
                    placeholder={this.form.o}
                    placeholderTextColor='#fff'
                    inputContainerStyle={styles3.input}
                    inputStyle={{color:'#fff',textAlign:'center'}}
                    onChangeText={(text) => this.setState( prevState =>({
                        client:{
                            ...prevState.client,
                            Отчество:text
                        }
                    }))}
                    errorMessage={this.state.errors.Отчество}
                />
                <Input
                    keyboardType="phone-pad"
                    containerStyle={{alignItems:'center'}}
                    placeholder={this.form.phone}
                    placeholderTextColor='#fff'
                    inputContainerStyle={styles3.input}
                    inputStyle={{color:'#fff',textAlign:'center'}}
                    onChangeText={(text) => this.setState( prevState =>({
                        client:{
                            ...prevState.client,
                            phone:text
                        }
                    }))}
                    errorMessage={this.state.errors.phone}
                />
                <TouchableOpacity style={[styles3.input,{justifyContent :'center'}]} onPress={this._showDateTimePicker}>
                    <Text style={{color:'#fff',textAlign:'center',fontSize:17}}>{this.state.client.ДатаРождения}</Text>
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                />
                <View style={styles3.pickerContainer}>
                    <RNPickerSelect
                        placeholderTextColor = '#fff'
                        onValueChange={(itemValue) =>
                            this.setState( prevState =>({
                                client:{
                                    ...prevState.client,
                                    Пол:itemValue
                                }
                            }))
                        }
                        items={sexes}
                        placeholder={placeholder}
                        value={this.state.client.Пол}
                        useNativeAndroidPickerStyle={false}
                        style={pickerStyle}
                    />
                </View>
                <Text style={styles3.underInput}>{this.form.con}</Text>
                <TouchableOpacity onPress={()=>{Linking.openURL('http://thetaself.com/privacy')}}>
                    <Text style={[styles3.underInput,{textDecorationLine:'underline', textWeight:'bold'}]}>{this.form.pol}</Text>
                </TouchableOpacity>
                <Button
                    title={this.form.finish}
                    onPress={this._validate}
                    buttonStyle={styles3.button}
                    titleStyle={styles3.btnText}
                    containerStyle={styles3.btnContainer}
                />
            </LinearGradient>
        );
    }
}

const AuthStack = createStackNavigator(
    {
        Start: StartScreen,
        Number: NumberScreen,
        Sms: SmsScreen,
        Register: RegScreen

    },
    {headerMode:'none'});

export default createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: MainTabNavigator,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'AuthLoading',
    }
));

import React from 'react';
import {View,
    StyleSheet,
    WebView,
    Modal,
    FlatList,
    Text
} from 'react-native';
import ListItem from '../../components/ListItem'
import {Localization,LinearGradient} from 'expo'

export default class VideoScreen extends React.Component {
    constructor(props){
        super(props);
        let info=props.navigation.getParam('info','');
        let html = '<html><body style="padding-top: 20px; background: linear-gradient(#5B2453, #252285);">' +
            '<div style="position: relative; width: 100%; height: 0; padding-bottom: 56.25%; align-items: center"; >' +
            '<iframe style="position: absolute; top:0; left: 0;" width="100%"; height="100%"; src="'+ info.url + '" frameborder="0" allowfullscreen=""></iframe></div>'+
            '<h1 style="text-align: center; color:#fff; font-size:16pt; font-weight:bold">'+info.title+'</h1>'+
            '<p style="text-align: center; font-size:12pt;color:#fff;">'+info.info+'</p>'+
            '</body></html>'
        this.state = {
            html:html
        }
    }
    render() {
        return(
            <LinearGradient style={{flex:1}}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <WebView   source={{html:this.state.html}}/>
            </LinearGradient>
        )
    }
}

styles = StyleSheet.create({
})
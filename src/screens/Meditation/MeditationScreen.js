import React from 'react';
import {View,
    StyleSheet,
    WebView,
    Modal,
    FlatList
} from 'react-native';
import ListItem from '../../components/ListItem'
import {Localization,LinearGradient} from 'expo'
export default class MeditationScreen extends React.Component {
    static navigationOptions = {
        title: Localization.locale==="ru-RU"? "Медитация":"Meditation",
        headerRight:''
    };
    state = {
        isPlaying: false
    };
    _onModalClose=()=>{
        this.setState((prevState)=>{
            return {
                ...prevState,
                isPlaying:false,
                url:''
            }
        })
    };
    onPress=(url)=>{
        /*this.setState((prevState)=>{
            return {
                ...prevState,
                isPlaying:true,
                html:this.getHtml(url)
            }
        })*/
        this.props.navigation.push('MeditationVideo',{
            info:url
        })
    };
    getHtml = (item) =>{
        let html = '<html><body style="padding-top: 20px"><h1 style="text-align: center; color:#A94EEF; font-size:20pt; font-weight:bold">'+item.title+'</h1>' +
            '<div style="position: relative; width: 100%; height: 0; padding-bottom: 56.25%; align-items: center"; >' +
            '<iframe style="position: absolute; top:56.25%; left: 0; width: 100%; height: 100%"; src="'+ item.url + '" frameborder="0" ' +
            'allowFullScreen ' +
            '></iframe><p style="text-align: center; font-size:12pt">'+item.info+'</p></div></body></html>'
        return html

    };
    urls=[
        {
            title:"Манифестация",
            url: "https://www.youtube.com/embed/Rb6-9OY8Dqk",
            info: "Психолог, сертифицированный инструктор Тета-хилинг Марина Хрусталёва 8-965-477-29-99 "
        },
        {
            title:"Практика принятия себя",
            url: "https://www.youtube.com/embed/E6bEAJm-a4E",
            info: "Психолог, сертифицированный инструктор Тета-хилинг Марина Хрусталёва 8-965-477-29-99 "
        },
        {
            title:"Медитация прощения",
            url: "https://www.youtube.com/embed/1pS6k5O8mdc",
            info: "Психолог, сертифицированный инструктор Тета-хилинг Марина Хрусталёва 8-965-477-29-99 "
        },
        {
            title:"Тета медитация Единство",
            url: "https://www.youtube.com/embed/Yuz9wkIPcUU",
            info: "Психолог, сертифицированный инструктор Тета-хилинг Марина Хрусталёва 8-965-477-29-99 "
        },
        {
            title:"Тета-медитация. Медитация Тета хилинг",
            url: "https://www.youtube.com/embed/HMaud2dmS-M",
            info: "Психолог, сертифицированный инструктор Тета-хилинг Марина Хрусталёва 8-965-477-29-99 "
        },
        {
            title:"Медитация тета-хилинг для очищения и наполнения любовью",
            url: "https://www.youtube.com/embed/99p-8OMIthI",
            info: "Медитация для очищения и наполнения любовью и целительной энергией себя и своих близких\n" +
            "\n" +
            "Тел 8914 - 295- 61- 44\n" +
            "сайт MarinaKhrustaleva.com "
        }
    ]
    render() {
        return (
            <LinearGradient style={{flex:1}}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <FlatList style={styles.container}
                          data={this.urls}
                          renderItem={
                              ({item})=>{
                                  return <ListItem
                                      itemImage = {false}
                                      itemText = {item.title}
                                      onItemPressed = {() => this.onPress(item)}
                                  />}
                          }
                />
                {/*<Modal  visible = {this.state.isPlaying} animationType="slide"  onRequestClose={this._onModalClose}>
                    <WebView  source={{html:this.state.html}}/>
                </Modal>*/}
            </LinearGradient>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 15
    },
});

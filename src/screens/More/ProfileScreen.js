import React from 'react';
import { StyleSheet,AsyncStorage,View,ScrollView,Text,Image,TouchableOpacity } from 'react-native';
import {Button,Input} from 'react-native-elements';
import { Localization,LinearGradient,Icon } from 'expo';
import i18n from 'i18n-js';
import ThetaButton from '../../components/ThetaButton'
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import DateTimePicker from 'react-native-modal-datetime-picker';
import RNPickerSelect from 'react-native-picker-select';

export default class ProfileScreen extends React.Component {
    constructor() {
        super();
    }
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'TetaSelf'),
        };
    };
    _onExit = async () => {
            try {
                await AsyncStorage.setItem('userToken','');
                await AsyncStorage.setItem('User','');
                this.props.navigation.navigate('AuthLoading');
            } catch (error) {
            }
        };
    _onEdit = (user)=>{
        this.props.navigation.navigate('Edit',{
            user:user
        })
    }
    render() {
        const user = this.props.navigation.getParam('user','user error');
        return (
            <LinearGradient style={styles.main}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <Image source={require('../../assets/images/avatar.png')} style={styles.image} resizeMode = 'contain' />
                <ScrollView style={styles.container}>
                    <View style={styles.row}>
                        <Text style={styles.item}>{i18n.t('family')}</Text>
                        <Text style={styles.value}>{user.Фамилия}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.item}>{i18n.t('name')}</Text>
                        <Text style={styles.value}>{user.Имя}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.item}>{i18n.t('father')}</Text>
                        <Text style={styles.value}>{user.Отчество}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.item}>{i18n.t('birthdate')}</Text>
                        <Text style={styles.value}>{user.ДатаРождения}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.item}>{i18n.t('sex')}</Text>
                        <Text style={styles.value}>{user.Пол}</Text>
                    </View>
                </ScrollView>
                <ThetaButton
                    onPress = {()=>this._onEdit(user)}
                    title={i18n.t('edit')}
                />
                <ThetaButton
                    onPress = {this._onExit}
                    title={i18n.t('logout')}
                />
            </LinearGradient>
        );
    }
}
const sexesRu = [
    {
        label: 'Муж.',
        value: 'муж',
        color: '#000'
    },
    {
        label: 'Жен.',
        value: 'жен',
        color: '#000'
    },

];
const sexesEn = [
    {
        label: 'Male',
        value: 'муж',
        color: '#000'
    },
    {
        label: 'Female',
        value: 'жен',
        color: '#000'
    },

];
sexes = Localization.locale==="ru-RU"?sexesRu:sexesEn;
export class EditScreen extends React.Component {
    constructor(props){
        super(props);
        let user = props.navigation.getParam('user','');
        this.state = {
            isDateTimePickerVisible: false,
            client:{
                Фамилия: user.Фамилия,
                Имя: user.Имя,
                Отчество:user.Отчество,
                ДатаРождения:user.ДатаРождения,
                Пол:user.Пол
            },
            errors:{}
        };
    }
    _showDateTimePicker = () => this.setState(
        {
            isDateTimePickerVisible: true
        });

    _hideDateTimePicker = () => {
        this.setState(
            {
                isDateTimePickerVisible: false
            })};


    _handleDatePicked = (date) => {
        this.setState( prevState =>({
            client:{
                ...prevState.client,
                ДатаРождения:date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()
            }
        }));
        this._hideDateTimePicker();
    };
    _validate = ()=>{
        let errors = {};
        let empty = true;
        for(let key in this.state.client){
            if(this.state.client[key]===''){
                empty = false;
                errors[key] = 'Введите '+key;
                if(key==='phone') errors[key] = "Введите номер"
            }
        }
        if (!empty) this.setState(prevState=>({
            ...prevState,
            errors:errors
        }));
        else this._signInAsync();
    };
    _signInAsync = async () => {
        /*const newToken = await fetch('http://app.thetaself.com/users/sign', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'content-type': 'application/json'
            },
            body: JSON.stringify(
                this.state.client
            )
        })
            .then((response)=>response.json())
            .catch((error) =>{
            });
        await console.log(newToken);
        if(newToken.phone) this.setState(prevState=>({
            ...prevState,
            errors:{
                phone:'Этот номер уже занят'
            }
        }))
        else {
            await AsyncStorage.setItem('userToken', newToken);
            await AsyncStorage.setItem('User',JSON.stringify(this.state.client));
            await this.props.navigation.navigate('Settings');
        }*/
    };
    formRu={
        first:"Ваши профиль",
        input:"Заполните форму ниже," +
        "чтобы изменить данные",
        f:"Фамилия",
        i:"Имя",
        o:"Отчество",
        bd:"Дата Рождения",
        phone:"Номер телефона",
        finish:"Сохранить данные"
    };
    formEn={

        first: "Your profile",
        input: "Fill out the form below" +
        "to change your data",
        f: "Last Name",
        i: "Name",
        o: "Patronymic",
        bd: "Date of Birth",
        phone:"Phone number",
        finish: "Save data"
    };
    form = Localization.locale==="ru-RU"? this.formRu:this.formEn;
    render() {
        const placeholder = {
            label: Localization.locale==="ru-RU"?'Выберите пол':'Choose gender',
            value: null,
        };
        return (
            <LinearGradient style={styles3.mainContent}
                            colors={['#5B2453','#252285']}
                            start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            >
                <Text style={styles3.title}>{this.form.first}</Text>
                <Text style={styles3.label}>{this.form.input}</Text>
                <Input
                    containerStyle={{alignItems:'center'}}
                    value={this.state.client.Имя}
                    placeholder={this.form.i}
                    placeholderTextColor='#fff'
                    inputContainerStyle={styles3.input}
                    inputStyle={{color:'#fff',textAlign:'center'}}
                    onChangeText={(text) => this.setState( prevState =>({
                        client:{
                            ...prevState.client,
                            Имя:text
                        }
                    }))}
                    errorMessage={this.state.errors.Имя}
                />
                <Input
                    value={this.state.client.Фамилия}
                    containerStyle={{alignItems:'center'}}
                    placeholder={this.form.f}
                    placeholderTextColor='#fff'
                    inputContainerStyle={styles3.input}
                    inputStyle={{color:'#fff',textAlign:'center'}}
                    onChangeText={(text) => this.setState( prevState =>({
                        client:{
                            ...prevState.client,
                            Фамилия:text
                        }
                    }))}
                    errorMessage={this.state.errors.Фамилия}
                />
                <Input
                    value={this.state.client.Отчество}
                    containerStyle={{alignItems:'center'}}
                    placeholder={this.form.o}
                    placeholderTextColor='#fff'
                    inputContainerStyle={styles3.input}
                    inputStyle={{color:'#fff',textAlign:'center'}}
                    onChangeText={(text) => this.setState( prevState =>({
                        client:{
                            ...prevState.client,
                            Отчество:text
                        }
                    }))}
                    errorMessage={this.state.errors.Отчество}
                />
                <TouchableOpacity style={[styles3.input,{justifyContent :'center'}]} onPress={this._showDateTimePicker}>
                    <Text style={{color:'#fff',textAlign:'center',fontSize:17}}>{this.state.client.ДатаРождения}</Text>
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                />
                <View style={styles3.pickerContainer}>
                    <RNPickerSelect
                        placeholderTextColor = '#fff'
                        onValueChange={(itemValue) =>
                            this.setState( prevState =>({
                                client:{
                                    ...prevState.client,
                                    Пол:itemValue
                                }
                            }))
                        }
                        items={sexes}
                        placeholder={placeholder}
                        value={this.state.client.Пол}
                        useNativeAndroidPickerStyle={false}
                        style={pickerStyle}
                    />
                </View>
                <Button
                    title={this.form.finish}
                    onPress={this._validate}
                    buttonStyle={styles3.button}
                    titleStyle={styles3.btnText}
                    containerStyle={styles3.btnContainer}
                />
            </LinearGradient>
        );
    }
}

const styles = StyleSheet.create({
    main:{
        flex:1,
        alignItems:'center',
        paddingBottom:20
    },
    container:{
        flex: 1,
        marginBottom: 40,
        alignItems:"center",
    },
    image:{
        height:150,
        width:150,
        margin:5,
    },
    row:{
        flexDirection: 'row',
        fontSize: 14,
        marginTop:5
    },
    item:{
        textAlign:'right',
        color: '#fff',
        width:100
    },
    value:{
        textAlign:'left',
        color:'#A94EEF',
        marginLeft: 10,
    },
    button:{
        width: 200,
        height:40,
        fontSize:14,
        borderRadius:20,
        marginBottom: 40,
        backgroundColor:"#ff0000"
    },
    question:{
        textAlign:'center',
        fontSize:14,
        marginBottom: 10
    }
});
const styles3 = StyleSheet.create({
    mainContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnContainer:{
        backgroundColor:'#fff',
        alignItems:'center',
        borderRadius:25,
        marginTop: 20,
    },
    button: {
        alignItems:'center',
        backgroundColor:'#fff',
        borderRadius:25,
        width:wp('70%'),
        height:40,
    },
    btnText:{
        fontSize:17,
        color :'#252285',
        textAlign: 'center'
    },
    label:{
        width:wp('40%'),
        marginBottom:20,
        textAlign:'center',
        color:'#fff',
        fontSize:12
    },
    title:{
        marginBottom:20,
        textAlign:'center',
        color:'#fff',
        fontSize:20
    },
    input:{
        width:wp('70%'),
        height:40,
        backgroundColor:"#573578",
        borderRadius:20,
        fontSize:17,
        alignItems:'center',
        borderBottomWidth: 0,
        marginBottom:10
    },
    image:{
        marginBottom:30,
        width:wp('12,5%'),
        height:wp('12,5%')
    },
    icon:{
        position:'absolute',
        top:30,
        left:10
    },
    underInput:{
        width:wp('60%'),
        textAlign:'center',
        color:'#fff',
        fontSize:12
    },
    pickerContainer:{
        width:wp('70%'),
        height:40,
        backgroundColor:"#573578",
        borderRadius:20,
        alignItems:'center',
        justifyContent:'center',
        borderBottomWidth: 0,
        marginBottom:10,
        fontSize:17,
        textAlign:'center',
        color: "#fff"
    },
    picker :{
        fontSize:17,
        textAlign:'center',
        color: "#fff"
    },
});
const pickerStyle = StyleSheet.create({
    inputAndroid: {
        alignItems : 'center',
        fontSize: 17,
        color: '#fff',
        textAlign: 'center'
    },
})
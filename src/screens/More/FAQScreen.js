import React from 'react';
import {FlatList, StyleSheet,  Text, View, Platform} from 'react-native';
import { Localization,LinearGradient } from 'expo';
import i18n from 'i18n-js';
questions=[
    {
        text:"Как дела?",
        answer: "Хорошо"
    },
    {
        text:"А у тебя?",
        answer: "Тоже хорошо"
    },
    {
        text:"ТетаХилинг помогает?",
        answer:"Очень"
    },
    {
        text:"Тебе нравится ТетаХилинг",
        answer:"Да"
    }
];
const en1={
    title:"FAQ"
}
const ru1={
    title:'Ответы на вопросы',
}
questions2=[
    {
        text:"How are you?",
        answer: "Fine"
    },
    {
        text:"And you?",
        answer: "So,am I"
    },
    {
        text:"Does Teta Healing help?",
        answer:"It's very helpful"
    },
    {
        text:"Do you like it?",
        answer:"Sure"
    }
]

export default class FAQScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'TetaSelf'),
        };
    };
    render() {
        return <LinearGradient style={{flex:1}}
                               colors={['#5B2453','#252285']}
                               start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
        ><FlatList style={styles.container}
                         data={Localization.locale==="ru-RU"? questions:questions2}
                         renderItem={
                             ({item}) => {
                                 return <View style={styles.listItem}>
                                         <Text style={styles.question}>{item.text}</Text>
                                         <Text style={styles.answer}>{item.answer}</Text>
                                     </View>
                             }
                         }
                />
        </LinearGradient>
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    question:{
        fontSize:16,
        fontWeight:"bold",
        color:'#fff'
    },
    answer:{
        fontSize:16,
        fontStyle:"italic",
        color:'#fff'
    },
    listItem: {
        margin:10,
        width: "100%",
        height: 80,
    },

})

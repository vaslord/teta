import React from 'react';
import {ScrollView, StyleSheet, Image, TouchableOpacity, Text, View, AsyncStorage} from 'react-native';
import ListItem from '../../components/ListItem'
import { Localization,LinearGradient } from 'expo';
import i18n from 'i18n-js';

export default class MoreScreen extends React.Component {
    constructor() {
        super();
        this.state = {
            isLoading : true,
        };
        this._loadAsync();
    }
    images = [
        require("../../assets/icons/other-icon-1.png"),
        require("../../assets/icons/other-icon-2.png"),
        require("../../assets/icons/other-icon-3.png"),
        require("../../assets/icons/other-icon-4.png"),
        require("../../assets/icons/other-icon-5.png"),
    ]
    static navigationOptions = {
        title: Localization.locale==="ru-RU"? "Ещё":"More",
        headerRight:''
    };
    _loadAsync =async() =>
    {
        try{
            const user = await AsyncStorage.getItem('User');
            await this.setState({
                isLoading: false,
                user:JSON.parse(user)
            })
        }
        catch (err) {

        }
    };
    _toProfile = () =>{
        this.props.navigation.navigate('Profile',{
            user:this.state.user,
            title:i18n.t('profile')
        })
    };
    _toAbout = () =>{
        this.props.navigation.navigate('About',{
            title:i18n.t('about')
        })
    };
    _toFAQ = () =>{
        this.props.navigation.navigate('FAQ',{
            title:i18n.t('faq')
        })
    };
    _toMySub = () =>{
        this.props.navigation.push('Works',{
            title:i18n.t('mySub')
        })
    };
    render() {
            if(!this.state.isLoading) return (
                <LinearGradient style={{flex:1,paddingLeft:13}}
                                colors={['#5B2453','#252285']}
                                start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
                >
                    <ScrollView style={styles.container}>
                        <TouchableOpacity onPress={this._toProfile}>
                            <View style={styles.profile}>
                                <Image source={require('../../assets/images/avatar.png')} style ={styles.avatar}/>
                                <View style={styles.about}>
                                    <Text style={{fontSize:16, fontWeight:"bold",color:"#fff"}}>{this.state.user.Фамилия+' '+this.state.user.Имя}</Text>
                                    <Text style={{fontSize:16, color:"#fff"}}>{i18n.t('toProfile')}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <ListItem
                            itemText = {i18n.t('mySub')}
                            itemImage = {this.images[0]}
                            onItemPressed = {this._toMySub}
                            size={26}
                        />
                        <ListItem
                            itemText = {i18n.t('feedbacks')}
                            itemImage = {this.images[1]}
                            onItemPressed = {()=>{this.props.navigation.navigate('Feedbacks')}}
                            size={26}
                        />
                        <ListItem
                            itemText = {i18n.t('faq')}
                            itemImage = {this.images[3]}
                            onItemPressed = {this._toFAQ}
                            size={26}
                        />
                        <ListItem
                            itemText = {i18n.t('about')}
                            itemImage = {this.images[4]}
                            onItemPressed = {this._toAbout}
                            size={26}
                        />
                    </ScrollView>
                </LinearGradient>
            );
        else return <View/>
        ;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
    },
    avatar:{
        width: 60,
        height: 60
    },
    profile:{
        flexDirection:"row",
        width:"70%",
        height:60
    },
    about:{
        flexDirection:"column",
        justifyContent:"space-between",
        marginLeft: 15,
        paddingVertical:4,
        textSize:16
    }
});

import React, { Component } from "react";
import {StyleSheet, KeyboardAvoidingView, FlatList, AsyncStorage} from "react-native";
import Feedback from '../../components/Feedback'
import {LinearGradient} from 'expo'
import {Header} from "react-navigation";
import {Input} from 'react-native-elements';
export default class FeedbacksScreen extends Component {
    state = {
        isLoading:true
    };
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'TetaSelf'),
        };
    };
    async componentDidMount(){
        const userToken = await AsyncStorage.getItem('userToken');
        const base64 = require('base-64');
        await fetch(
            'http://app.thetaself.com/feedbacks/feeds', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Basic '+ base64.encode(userToken+":")
                }
            }
        )
            .then((response) => response.json())
            .then((responseJson) => {

                this.setState({
                    isLoading: false,
                    feedbacks: responseJson,
                }, function(){

                });
            })
            .catch((error) =>{

            });
    }
    render() {
        if(!this.state.isLoading)
            return (
                <LinearGradient style={{flex:1}}
                                colors={['#5B2453','#252285']}
                                start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
                >
                    <KeyboardAvoidingView  style = {{flex:1}} behavior='padding' enabled keyboardVerticalOffset = {Header.HEIGHT + 20}>
                    <FlatList
                        style={styles.container}
                        data={this.state.feedbacks}
                        renderItem={({item})=>
                            <Feedback
                                itemImage={require('../../assets/images/feed.png')}
                                text={item.text}
                                name={item.name}
                            />
                        }
                    />
                        {/*<Input
                        placeholder='Введите ваш отзыв'
                        containerStyle={{width:'100%', height:52}}
                        inputStyle={styles.input}
                        inputContainerStyle={{flex:1,borderBottomWidth: 0}}
                    />*/}
                    </KeyboardAvoidingView>
                </LinearGradient>
            )
        else
            return <LinearGradient style={{flex:1,padding:10}}
                                   colors={['#5B2453','#252285']}
                                   start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
            />
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    input:{
        flex:1,
        backgroundColor:'#514E9D',
        paddingHorizontal:15,
        color:'#fff',
        fontSize:14,
        borderWidth:0
    }

});
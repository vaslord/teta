import React from 'react';
import {  StyleSheet,Text,View } from 'react-native';
import i18n from 'i18n-js';
import {LinearGradient} from 'expo'
export default class AboutScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'TetaSelf'),
        };
    };
    render() {
        return <LinearGradient style={{flex:1,padding:10}}
                               colors={['#5B2453','#252285']}
                               start={{x: 0.5, y: 0}} end={{x: 0.5, y: 1}}
        >
        <View style={styles.container}>
            <Text style={{margin:5,fontSize:14,fontWeight:'bold',color:'#fff'}}>ThetaSelf</Text>
            <Text style={{margin:5,fontSize:14,fontWeight:'light',color:'#fff'}}>{i18n.t('version')+" 1.0.1"+" 01.04.2019"}</Text>
            <Text style={{margin:5,fontSize:14,fontWeight:'light',color:'#fff'}}>{i18n.t('developed')}</Text>
            <Text style={{margin:5,fontSize:14,fontWeight:'light',color:'#fff'}}>{i18n.t('icons')}</Text>
            <Text style={{margin:5,fontSize:14,fontWeight:'light',color:'#fff'}}>{i18n.t('helpmail')+" help@thetaself.ocm"}</Text>
            <Text style={{margin:5,fontSize:14,color:"#0075CD"}}>{i18n.t('condition')}</Text>
            <Text style={{margin:5,fontSize:14,color:"#0075CD"}}>{i18n.t('polite')}</Text>
        </View>
        </LinearGradient>
    }
}

styles=StyleSheet.create({
    container:{
        flex:1,
        padding:5,
        paddingLeft:10
    },
    text:{
        textAlign:"center",
        color:"#A94EEF"
    }
})

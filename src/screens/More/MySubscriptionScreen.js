import React from 'react';
import {  StyleSheet,Text,View,AsyncStorage } from 'react-native';
import { Localization } from 'expo';
import i18n from 'i18n-js';



export default class MySubscriptionScreen extends React.Component {
    constructor() {
        super();
        this._loadAsync();
        this.state = {
            isLoading : true
        }
    }
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'TetaSelf'),
        };
    };
    _loadAsync =async() =>
    {
        try{
            var user = await AsyncStorage.getItem('User');
            await this.setState({
                isLoading: false,
                user:JSON.parse(user)
            })
        }
        catch (err) {
        }

    };
    _subStatus = (subBegin,subEnd) => {
          if (subBegin){
              return <View>
                  <View style={styles.row}>
                      <Text style={styles.item}>{i18n.t('start')}</Text>
                      <Text style={styles.value}>{subBegin}</Text>
                  </View>
                  <View style={styles.row}>
                      <Text style={styles.item}>{i18n.t('end')}</Text>
                      <Text style={styles.value}>{subEnd}</Text>
                  </View>
              </View>
          }
          else return <Text style={styles.item}>{i18n.t('nosub')}</Text>
    };
    render() {

        if (this.state.isLoading) return <View style={styles.container}>
            <Text style={styles.text}>Загрузка</Text>
        </View>
        else {
            return <View style={styles.container}>
                <Text style={styles.text}>{i18n.t('info')}</Text>
                {this._subStatus(this.state.user.ПлатнаяПодпискаНачало,this.state.user.ПлатнаяПодпискаКонец)}
            </View>
        }

    }
}

const styles=StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    },
    text:{
        fontSize:20,
        textAlign:"center",
        color:"#A94EEF"
    },
    row:{
        flexDirection: 'row',
        fontSize: 14,
    },
    item:{
        color: '#383838'
    },
    value:{
        color:'#A94EEF',
        marginLeft: 10,
    }

})

const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#D1D3D4',
  tabIconSelected: "#FFFFFF",
  tabBar: '#FF00FF',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#FF00FF',
};

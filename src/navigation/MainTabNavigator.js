import React from 'react';
import { View} from 'react-native';
import { createStackNavigator, createBottomTabNavigator,BottomTabBar } from 'react-navigation';
import HomeScreen from '../screens/Healers/HomeScreen';
import Works from '../screens/Works/Works';
import Setups from '../screens/Setups/Setups';
import MeditationScreen from '../screens/Meditation/MeditationScreen'
import MoreScreen from '../screens/More/MoreScreen'
import SpheresScreen from '../screens/Setups/SpheresScreen'
import {LogoIcon,HealerIcon,DnaIcon,FlowerIcon,MoreIcon} from '../components/Icons'
import MyWorksScreen from '../screens/Setups/Downloads/MyWorksScreen'
import MyWorksModal from '../screens/Setups/Downloads/MyWorksModal'
import Relationships from '../screens/Setups/Relationships/Relationships'
import ParentsScreen from '../screens/Setups/Relationships/ParentsScreen'
import EnterModal from '../screens/Setups/EnterModal'
import TestModal from '../screens/Setups/TestModal'
import HealerScreen from "../screens/Healers/HealerScreen";
import HealerModal from "../screens/Healers/HealerModal";
import { LinearGradient } from 'expo';
import MyWorksModal2 from "../screens/Setups/Downloads/MyWorksModal2";
import BeforeVideoModal from "../screens/Setups/beforeVideoModal";
import VideoModal from "../screens/Setups/VideoModal";
import YesModal1 from "../screens/Setups/YesModal1";
import InstructModal1 from "../screens/Setups/InstructModal1";
import TestModal2 from "../screens/Setups/TestModal2";
import FinishModal from "../screens/Setups/FinishModal";
import FeedbacksScreen from "../screens/More/FeedbacksScreen";
import ProfileScreen, {EditScreen} from "../screens/More/ProfileScreen";
import FAQScreen from "../screens/More/FAQScreen";
import AboutScreen from "../screens/More/AboutScreen";
import i18n from 'i18n-js';
import {Localization} from 'expo'
import subCat1 from "../screens/Setups/subCat1";
import subCat2 from "../screens/Setups/subCat2";
import subCat3 from "../screens/Setups/subCat3";
import subCat4 from "../screens/Setups/subCat4";
import EndModal from "../screens/Setups/EndModal";
import BuyScreen from "../screens/Works/BuyScreen";
import subCat2a from "../screens/Setups/subCat2a";
import Downloads from "../screens/Setups/Downloads/Main";
import BuyFull from "../screens/Works/BuyFull";
import VideoScreen from "../screens/Meditation/VideoScreen";
import HealthScreen from "../screens/Setups/Relationships/Health";

const Modals=['EnterModal','TestModal',
    'TestModal2','MyModal','MyWorksModal2','FinishModal',
    'HealerModal','BeforeVideoModal','VideoModal','YesModal1',
    'InstructModal1','Works','Buy',
    'Settings','subCat1','subCat2','subCat3','subCat4','EndModal','Health'
];

const HomeStack = createStackNavigator(
    {
        Home: HomeScreen,
        HealerInfo: HealerScreen
    },
    {
        defaultNavigationOptions: {
            headerStyle:{
                backgroundColor:"#5B2453"
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                flex:1,
                fontWeight: 'bold',
                textAlign: 'center',alignSelf:'center'
            },
            headerRight:(
                <View></View>
            )

    },
    }
);

const RootHomeStack = createStackNavigator(
    {
        Main: {
            screen: HomeStack,
        },
        HealerContacts: {
            screen: HealerModal,
            navigationOptions: {
                tabBarVisible:false
            }
        },
    },
    {
        mode: 'modal',
        headerMode: 'none',
    }
);

RootHomeStack.navigationOptions = ({ navigation }) => {
    let { routeName } = navigation.state.routes[navigation.state.index];
    let navigationOptions = {
        tabBarLabel: i18n.t('tabBar1'),
        tabBarIcon: ({focused  }) => {
            return <HealerIcon focused ={focused}/>
        },
    };

    if (routeName === 'HealerContacts') {
        navigationOptions.tabBarVisible = false;
    }

    return navigationOptions;
};
const MainWorksStack = createStackNavigator({
    BuyFull: BuyFull,
},
    {
        initialRouteName: "BuyFull",
        defaultNavigationOptions: {
            headerStyle:{
                backgroundColor:"#5B2453"
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                flex:1,
                fontWeight: 'bold',
                textAlign: 'center',alignSelf:'center'
            },
            headerRight:(
                <View></View>
            )

        },
    }
    );
const RootWorksStack = createStackNavigator(
    {
        Main: {
            screen: MainWorksStack,
        },
        Buy:{
            screen: BuyScreen
        },
        Works:{
            screen:Works
        }
    },
    {
        mode: 'modal',
        headerMode: 'none',
    }
);

RootWorksStack.navigationOptions = ({ navigation }) => {
    let { routeName } = navigation.state.routes[navigation.state.index];
    let navigationOptions = {
        tabBarLabel: i18n.t('tabBar2'),
        tabBarIcon: ({ focused }) => {
            return <DnaIcon focused ={focused}/>
        },
    };
    if (Modals.find(value => value===routeName )) {
        navigationOptions.tabBarVisible = false;
    }

    return navigationOptions;
};
const SetupsStack = createStackNavigator({
        Spheres: SpheresScreen,
        Relationships : Relationships,
        Parents: ParentsScreen,
        MyWorks: MyWorksScreen,
        Downloads: Downloads,
    },
    {
        initialRouteName: "Spheres",
        defaultNavigationOptions: {
            headerStyle:{
                backgroundColor:"#5B2453"
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                flex:1,
                fontWeight: 'bold',
                textAlign:"center",
            },
            headerRight:(
                <View></View>
            )
        },
    });
const RootSetupsStack = createStackNavigator(
{
    Main: {
        screen: SetupsStack
    },
    Settings: {
        screen: Setups
    },
    EnterModal: {
        screen: EnterModal
    },
    TestModal:{
        screen: TestModal
    },
    TestModal2:{
        screen: TestModal2
    },
    BeforeVideoModal:{
        screen: BeforeVideoModal
    },
    VideoModal:{
        screen: VideoModal
    },
    YesModal1:{
        screen: YesModal1
    },
    InstructModal1:{
        screen: InstructModal1
    },
    FinishModal:{
        screen: FinishModal
    },
    subCat1: {
        screen: subCat1
    },
    subCat2: {
        screen: subCat2
    },
    subCat2a: {
        screen: subCat2a
    },
    subCat3: {
        screen: subCat3
    },
    subCat4:{
        screen: subCat4
    },
    EndModal:{
        screen: EndModal
    },
    MyModal: {
        screen: MyWorksModal,
    },
    MyWorksModal2:{
        screen: MyWorksModal2
    },
    Health: HealthScreen
},
{
    mode: 'modal',
    headerMode: 'none',
}
);
RootSetupsStack.navigationOptions = ({ navigation }) => {
    let { routeName } = navigation.state.routes[navigation.state.index];
    let navigationOptions = {
        tabBarLabel: Localization.locale==='ru-RU'?"Установки":"Attitudes",
        tabBarIcon: ({ focused }) => {
            return <LogoIcon focused ={focused}/>
        },
    };
    if (Modals.find(value => value===routeName )) {
        navigationOptions.tabBarVisible = false;
    }
    return navigationOptions;
};

const MeditationStack = createStackNavigator({
    Meditation: MeditationScreen,
        MeditationVideo:VideoScreen
},
    {
        defaultNavigationOptions: {
            headerStyle:{
                backgroundColor:"#5B2453"
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                flex:1,
                fontWeight: 'bold',
                textAlign:"center",
            },
            headerRight:(
                <View></View>
            )
        },
    });

MeditationStack.navigationOptions = {
    tabBarLabel: Localization.locale==="ru-RU"? "Медитация":"Meditation",
    tabBarIcon: ({ focused }) => {
            return <FlowerIcon focused ={focused}/>
        },
};

const MoreStack = createStackNavigator({
    More: MoreScreen,
    Feedbacks:FeedbacksScreen,
    Profile:ProfileScreen,
    FAQ:FAQScreen,
    About:AboutScreen,
    Edit:EditScreen
},
    {
        defaultNavigationOptions: {
            headerStyle:{
                backgroundColor:"#5B2453"
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                flex:1,
                fontWeight: 'bold',
                textAlign:"center"
            },
            headerRight:(
                <View></View>
            )
        },
    });

MoreStack.navigationOptions = {
    tabBarLabel: Localization.locale==="ru-RU"? "Ещё":"More",
    tabBarIcon: ({ focused }) => (
        <MoreIcon focused ={focused}/>
    ),
};

const TabBarComponent = (props) => (
    <LinearGradient
    colors={['#C272ff', '#947BFF']}
    start={[0, 0]}
    end={[1, 1]}>
        <BottomTabBar {...props} />
    </LinearGradient>);
export default createBottomTabNavigator({
        RootHomeStack,
        RootWorksStack,
        RootSetupsStack,
        MeditationStack,
        MoreStack,

},
    {
        initialRouteName: "RootSetupsStack",
        /*tabBarComponent: props =>
            <TabBarComponent
                {...props}
                style={{ backgroundColor: 'Transparent'}}
            />,*/
        tabBarOptions: {
            activeTintColor: '#FFF',
            inactiveTintColor: '#9291C2',
            style: {
                paddingTop:5,
                backgroundColor: '#252285',
            },
            labelStyle:{
                fontSize:10,
            }
        }
    }
);


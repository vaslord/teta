import {SELECT_HEALER,DESELECT_HEALER } from './actionTypes'

export const selectHealer = (key) => {
    return {
        type: SELECT_HEALER,
        placeKey: key
    };
};

export const deselectHealer = () => {
    return {
        type: DESELECT_HEALER
    };
};


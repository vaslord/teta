import {SELECT_HEALER,DESELECT_HEALER} from "../actions/actionTypes"


const initialState = {
    healers: [
        {
            key: 1,
            name: action.healerName,
            image: require('../../assets/images/healer.png')
        }
],
    selectedHealer: null
};

const reducer = (state = initialState,action) => {
    switch (action.type) {
        case SELECT_HEALER:
            return {
                ...state,
                selectedHealer: state.healers.find(healer => {
                    return healer.key === action.healerKey;
                })
            };
        case DESELECT_HEALER:
            return {
                ...state,
                selectedHealer: null
            };
        default:
            return state;
    }
};

export default reducer;